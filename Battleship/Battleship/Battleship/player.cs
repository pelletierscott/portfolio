﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Battleship
{
    internal class player
    {
        //Declare variables
        public int number;
        public string source;
        Stack targetedShips = new Stack();
        Dictionary<int, Ship> knownPoints = new Dictionary<int, Ship>();
        char[] grid;
        int grid_height = 9;
        int grid_width = 11;
        int shotsLeft = 0;

        //Create players Ships
        Ship[] ships = new Ship[]
        {
            new Ship("Destroyer", new int[2]),
            new Ship("Cruiser", new int[3]),
            new Ship("Submarine", new int[3]),
            new Ship("Battleship", new int[4]),
            new Ship("Carrier", new int[5]),
        };

        //Construnct Player
        public player(int number, string source)
        {
            this.number = number;
            this.source = source;
        }

        //Initialize (Root)
        internal void init()
        {
            //Initialize player based on type
            switch (source)
            {
                //Initialize Human player
                case ("human"):
                    init_human();
                    break;

                //Initialize Machine Player
                case ("machine"):
                    init_machine();
                    break;
            }
        }

        //Initialize Human Player
        private void init_human()
        {
            //Generate Grid for ship placement
            grid = new char[grid_height * grid_width];
            for (int i = 0; i < grid.Length; i++)
            {
                grid[i] = ' ';
            }

            //Place each Ship
            foreach (Ship ship in ships)
            {
                //Set default position
                int coord = grid.Length / 2;
                bool orientation = true;
                bool shipPlaced = false;

                //Loop while Ship is not placed 
                while (!shipPlaced)
                {
                    //Convert position to row/column 
                    int row = (coord) / grid_width;
                    int column = (coord) % grid_width;

                    //Define position limits for ship size
                    int vertical_limit = (orientation) ? (grid_height - 1) : (grid_height - ship.coords.Length);
                    int horizontal_limit = (orientation) ? (grid_width - ship.coords.Length) : (grid_width - 1);

                    //Force row and colum into valid ranges
                    row = (row > vertical_limit) ? vertical_limit : row;
                    column = (column > horizontal_limit) ? horizontal_limit : column;

                    //Convert row and column back into position
                    coord = (row * grid_width) + column;

                    //Re-map ship position
                    for (int i = 0; i < ship.coords.Length; i++)
                    { ship.coords[i] = orientation ? coord + i : coord + (i * grid_width);   }

                    //Re-draw display
                    Console.Clear();
                    displayGrid(grid, ship);

                    //Get user input
                    ConsoleKeyInfo key = Console.ReadKey();

                    //Interprete user input
                    switch (key.Key)
                    {
                        case (ConsoleKey.UpArrow):
                            row = (row <= 0) ? 0 : (row - 1);
                            break;

                        case (ConsoleKey.DownArrow):
                            row = (row >= vertical_limit) ? vertical_limit : (row + 1);
                            break;

                        case (ConsoleKey.LeftArrow):
                            column = (column <= 0) ? 0 : (column - 1);
                            break;

                        case (ConsoleKey.RightArrow):
                            column = (column >= horizontal_limit) ? horizontal_limit : (column + 1);
                            break;

                        case (ConsoleKey.Spacebar):
                            //Change ship orientation
                            orientation = !orientation;
                            break;

                        case (ConsoleKey.Enter):
                            //Lock-in Ship position
                            shipPlaced = !shipPlaced;
                            break;
                    }

                    //Convert row and column back into position
                    coord = (row * grid_width) + column;

                    //Test if ship place is locked
                    if (shipPlaced)
                    {
                        //Loop through ships positions
                        foreach (int i in ship.coords)
                        {
                            //Test grid availibility
                            if (grid[i] != ' ')
                            {
                                if (grid[i] == '0')
                                {
                                    //Reject placement
                                    shipPlaced = false;
                                    Console.Beep();
                                    break;
                                }
                            }
                        }
                    }

                    //Test if ship place is locked
                    if (shipPlaced)
                    {
                        //Loop through ships positions
                        foreach (int i in ship.coords)
                        {
                            //Place ship on grid
                            grid[i] = '0';
                        }
                    }
                }
            }

            //Blank grid
            for (int i = 0; i < grid.Length; i++) 
            {
                grid[i] = ' ';
            }

            //Display post init information
            Console.Clear();
            System.Threading.Thread.Sleep(400);
            Console.WriteLine("\n\n\t   Player " + (number + 1) + " is Done");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("\t   Press Any Key to Continue");
            System.Threading.Thread.Sleep(400);
            Console.ReadKey();
        }

        //Initialize Machine Player
        private void init_machine()
        {
            //Declare variables
            Random random = new Random();

            //Initialize game board grid
            grid = new char[grid_height * grid_width];

            //Populate gameboard
            for (int i = 0; i < grid.Length; i++)
            {
                //Add black grid space
                grid[i] = ' ';
            }

            //Loop through each ship
            foreach (Ship s in ships)
            {
                //Clear console
                Console.Clear();

                //Set default check for ship placement
                bool shipPlaced = false;
                
                //Loop while ship placement is invalid
                while (!shipPlaced)
                {
                    //Generate random ship orientation
                    bool orientation = (random.NextDouble() >= 0.5) ? false : true;    //true = horizonatal

                    //Calculate min/max position of ship from orientation
                    int vertical_limit = (orientation) ? 0 : grid_height - s.coords.Length;
                    int horizontal_limit = (orientation) ? grid_width - s.coords.Length : grid_width;

                    //Generate random coordinate to place ship
                    int row = random.Next(0, vertical_limit);
                    int column = random.Next(0, horizontal_limit);

                    //Convert coords to grid refrence
                    int coord = (row * column) + column - 1;

                    //Set default check for grid availibility
                    bool availibility = true;

                    //Check each ship position for grid availibility
                    for (int i = 0; i < s.coords.Length; i++)
                    {
                        //assign position based on orientation
                        int pos = (orientation) ? (coord + i) : (coord + i * grid_width);

                        //Try ship position while they are in range of the grid
                        try
                        {
                            //Test if grid position is availible
                            if (grid[pos] != ' ')
                            {   availibility = false;   }
                        }
                        catch (IndexOutOfRangeException ex)
                        {   availibility = false;   }

                    }

                    //Test if all ship position where availible
                    if (availibility)
                    {
                        //Assign each ship position
                        for (int i = 0; i < s.coords.Length; i++)
                        {
                            int pos = (orientation) ? (coord + i) : (coord + i * grid_width);
                            grid[pos] = 'X';
                            s.coords[i] = pos;
                        }

                        //Ship placed succesfully
                        shipPlaced = true;
                    }
                }
            }
            
            //Print information and delay
            Console.Write("\n\n\t   Player " + (number + 1) +  " Ready");
            System.Threading.Thread.Sleep(2000);

            //Blank grid
            for (int i = 0; i < grid.Length; i++)
            {
                grid[i] = ' ';
            }

        }

        //Draw grid for placing ships
        private void displayGrid(char[] grid, Ship ship)
        {
            //Print Player Info
            Console.WriteLine("\n\n\t  Player " + (number + 1));

            //Print Ship details
            Console.WriteLine("\n\t  Place your " + ship.name + "(" + ship.coords.Length + ")");

            //Print Grid Top
            Console.Write("\n\n\n");
            Console.WriteLine("\t  ABCDEFGHIJK");
            Console.WriteLine("\t  -----------");

            //Set string for first line
            string str = "\t| ";

            //Work through all grid positions
            for (int i = 1; i <= grid.Length; i++)
            {
                //Set checker to defualt value (false)
                bool check = false;

                //Loop though all psotions of current ship
                foreach (int j in ship.coords)
                {
                    //Check if position matches grid position
                    if (j == i - 1)
                    {
                        //Set checker and escape loop
                        check = true;
                        break;
                    }
                }

                //Append string
                str += (check) ? '#' : grid[i - 1];

                //Test if loop has reached end of row
                if (i % grid_width == 0 && i != 0)
                {
                    //Write string and set next string
                    Console.WriteLine(str);
                    str = "\t| ";
                }
            }
        }

        //Draw grid for fireing shoots
        private void displayGrid(int peg)
        {
            //Print Player Info
            Console.WriteLine("\n\n\t  Player " + (number + 1));

            //Print Shots remaining
            Console.WriteLine("\n\t  You have " + shotsLeft + " shots remaining");

            //Print Grid Headers
            Console.Write("\n");
            Console.WriteLine("\t  ABCDEFGHIJK");
            Console.WriteLine("\t  -----------");

            //Add spacer
            string str = "\t| ";

            //Print grid rows
            for (int i = 0; i < grid.Length; i++)
            {
                if (i % (grid_width) == 0 && i != 0)
                {
                    Console.WriteLine(str);
                    str = "\t| ";
                }

                if (i == peg)
                {   str += '@'; }
                else
                {   str += grid[i]; }
            }
            Console.WriteLine(str);
        }

        //Run Turn (Root)
        internal bool Turn(player opponent)
        {
            //Run players turn based on type
            switch (source)
            {
                //Run Human Turn
                case ("human"):
                    return turn_human(opponent);

                //Run Machine Turn
                case ("machine"):
                    return turn_machine(opponent);

                default:
                    //Output Error
                    Console.WriteLine("error ");
                    Console.ReadKey();

                    //End Game
                    return true;
            }
        }

        //Run Humans Turn
        private bool turn_human(player opponent)
        {
            //Decalre variables
            shotsLeft = 0;
            int pos = grid.Length / 2;
            int row = 0;
            int column = 0;

            //Loop through each player's ship
            foreach (Ship ship in ships)
            {
                //Add shots if ship is alive/not Null
                shotsLeft += (ship == null) ? 0 : 1;
            }

            //Loop while shots remain
            while (shotsLeft > 0)
            {
                //Convert position to row/column
                row = (pos) / grid_width;
                column = (pos) % grid_width;

                //Re-draw display
                Console.Clear();
                displayGrid(pos);

                //Get user input
                ConsoleKeyInfo key = Console.ReadKey();

                //Interprete user input
                switch (key.Key)
                {
                    case (ConsoleKey.UpArrow):
                        row = (row - 1 < 0) ? 0 : (row - 1);
                        break;

                    case (ConsoleKey.DownArrow):
                        row = (row + 1 >= grid_height) ? (grid_height - 1) : (row + 1);
                        break;

                    case (ConsoleKey.LeftArrow):
                        column = (column - 1 < 0) ? 0 : (column - 1);
                        break;

                    case (ConsoleKey.RightArrow):
                        column = (column + 1 >= grid_width) ? grid_width - 1 : (column + 1);
                        break;

                    case (ConsoleKey.Enter):
                        //Test grid availibility
                        if (grid[pos] == ' ')
                        {
                            //Test shot at opponent
                            if (fireShot(pos, opponent) != null ? true : false)
                                {
                                //Add HIT to grid
                                grid[pos] = 'X';
                                Console.Write("\n\t   Hit!");
                                Console.Beep();
                            }
                            else
                            {
                                //Add MISS to grid
                                Console.Write("\n\t   Miss");
                                grid[pos] = '*';
                            }

                            //Deincrement shots remaining and delay
                            shotsLeft--;
                            System.Threading.Thread.Sleep(1000);
                        }
                        else
                        {
                            Console.Beep();
                        }
                        break;
                }

                //Convet row and column into position
                pos = (row * grid_width) + column;
            }

            //Display grid and information
            Console.Clear();
            displayGrid(-1);
            Console.WriteLine("\n\n\t   Next Players Turn");
            System.Threading.Thread.Sleep(1500);
            Console.WriteLine("\n\t   Press Any Key to Continue");
            Console.ReadKey();

            //Test and return game completion
            return gameOver(opponent);
        }

        //Run Machines Turn
        private bool turn_machine(player opponent)
        {
            //Decalre variables
            shotsLeft = 0;
            Random random = new Random();

            //Loop through each player's ship
            foreach (Ship ship in ships)
            {
                //Add shots if ship is alive/not Null
                shotsLeft += (ship == null) ? 0 : 1;
            }

            //Display grid
            Console.Clear();
            displayGrid(-1);
            System.Threading.Thread.Sleep(1000);

            //Loop while shots remain
            while (shotsLeft > 0)
            {
                //Declare values for tracking
                HashSet<int> targets = new HashSet<int>();
                HashSet<int> targetPoints = new HashSet<int>();
                Ship shipHit = null;
                bool validCoord = false;
                int pos = 0;

                //Test if any ships are being tracked
                if (knownPoints.Count == 0)
                {
                    //Add random value to target set
                    targetPoints.Add(random.Next(grid.Length));

                    //Test if 1st target set is valid
                    if (grid[targetPoints.First()] != '*' && grid[targetPoints.First()] != 'X')
                    {   validCoord = true;  }
                    else
                    {   validCoord = false; }  
                }
                else
                {
                    //Check if the first tracked ship is still alive
                    if (!knownPoints.Values.First().alive())
                    {
                        //Get tracked ship
                        Ship firstShip = knownPoints.Values.First();

                        //Get every tracked position 
                        var results = knownPoints.Where(pair => pair.Value == firstShip).Select(pair => pair.Key).ToArray();

                        //Sort tracked positions
                        Array.Sort(results);

                        //Add each result into datastructure
                        foreach (int i in results)
                        { targets.Add(i); }

                        //Add targets for if only one point is known
                        if (targets.Count == 1)
                        {
                            targetPoints.Add(targets.First() - grid_width);      //Up
                            targetPoints.Add(targets.First() + grid_width);      //Down
                            targetPoints.Add(targets.First() - 1);               //Left
                            targetPoints.Add(targets.First() + 1);               //Right
                        }
                        else
                        {
                            bool orientation = false;       //vertical   

                            //Get ship orientation from points
                            if ((targets.First() + 1 == targets.Skip(1).First()) || (targets.First() - 1 == targets.Skip(1).First()))
                            { orientation = true; }

                            //Assign targets based on orientation
                            if (orientation)
                            {
                                targetPoints.Add(targets.Min() - 1);               //Left
                                targetPoints.Add(targets.Max() + 1);               //Right
                            }
                            else
                            {
                                targetPoints.Add(targets.Min() - grid_width);      //Up
                                targetPoints.Add(targets.Max() + grid_width);      //Down
                            }
                        }

                        //Validate all target
                        for (int i = 0; i < targetPoints.Count; /**/)
                        {
                            //Test if target is out of bounds or occupied
                            if ((targetPoints.Skip(i).First() > 99) ||
                                (targetPoints.Skip(i).First() < 0) ||
                                (grid[targetPoints.Skip(i).First()] == '*') ||
                                (grid[targetPoints.Skip(i).First()] == 'X') ||
                                (grid[targetPoints.Skip(i).First()] % grid_width == 0) ||
                                (grid[targetPoints.Skip(i).First()] % grid_width == grid_width))
                            {
                                //Remove failed targets
                                targetPoints.Remove(targetPoints.Skip(i).First());
                            }
                            else
                            {
                                //Interate to next target
                                i++;
                            }
                        }

                        //Validate Target
                        validCoord = true;
                    }
                    else
                    {
                        //Get tracked ship to remove
                        Ship shipToRemove = knownPoints.First().Value;

                        //Check every tracked ship position
                        for (int i = 0; i < knownPoints.Count; /**/)
                        {
                            //Test knownPoint for match
                            if (knownPoints.Skip(i).First().Value == shipToRemove)
                            {
                                //Remove matching tracked ship position
                                knownPoints.Remove(knownPoints.Skip(i).First().Key);
                            }
                            else
                            {
                                //Check next tracked ship position
                                i++;
                            }
                        }
                        //Invalidate target
                        validCoord = false;
                    }
                }

                //Test if there are any targets set
                if (targetPoints.Count == 0)
                {
                    //Invalidate targetting
                    validCoord = false;
                }

                //Test if targetting is valid
                if (validCoord)
                {
                    //Select random value from target set
                    pos = targetPoints.Skip(random.Next(targetPoints.Count)).First();

                    //Check if shot hit a ship
                    shipHit = fireShot(pos, opponent);

                    //Test shot was sucessful
                    if (shipHit != null)
                    {
                        //Declare and display hit
                        shotsLeft--;
                        grid[pos] = 'X';
                        knownPoints.Add(pos, shipHit);
                        Console.Clear();
                        displayGrid(-1);

                        //Test if ship was hit or sunk
                        if (!shipHit.alive())
                        {   Console.WriteLine("\n\t   Hit!");   }
                        else
                        {   Console.WriteLine("\n\t   Enemy " + shipHit.name + " was Sunk");    }

                        //Delay
                        System.Threading.Thread.Sleep(1500);
                    }
                    else
                    {
                        //Declare and display miss
                        shotsLeft--;
                        grid[pos] = '*';
                        Console.Clear();
                        displayGrid(-1);
                        Console.WriteLine("\n\t   Miss");
                        System.Threading.Thread.Sleep(1500);
                    }
                }
            }

            //Display information and delay
            Console.Write("\n\t   Ready Next Player");
            System.Threading.Thread.Sleep(3000);

            //Test and return game completion
            return gameOver(opponent);
        }

        //Test shot againt opponents ships
        private Ship fireShot(int pos, player opponent)
        {
            //Loop through each enemy ship
            foreach (Ship ship in opponent.ships)
            {
                if (ship != null)
                {
                    //Test each ship's position
                    for (int i = 0; i < ship.coords.Length; i++)
                    {
                        //Test if shot matches ship position
                        if (pos == ship.coords[i])
                        {
                            //Adjust ship 
                            ship.coords[i] = -1;

                            //Check if ship was destroyed
                            testShip(opponent, ship);

                            //Return HIT
                            return ship;
                        }
                    }
                }
            }
            //Return MISS
            return null;
        }

        //Test and Modify destroyed enemy ship 
        private void testShip(player opponent, Ship ship)
        {
            //TEst if ship is alive
            if (ship.alive())
            {
                //Loop through each ship position
                for (int i = 0; i < ships.Length; i++)
                {
                    //Test if target ship is current ship
                    if (opponent.ships[i] == ship)
                    {
                        //Derefrence ship
                        opponent.ships[i] = null;

                        //Test if player is human
                        if (source == "human")
                        {
                            //Display information about sunk enemy ship
                            Console.WriteLine("\n\t   Enemy " + ship.name + " was Sunk");
                        }                       

                        //Delay
                        System.Threading.Thread.Sleep(1500);

                        //Test if player is human
                        if (source == "human")
                        {
                            //Wait for user input
                            Console.ReadKey();
                        }
                    }           
                }
            }
        }

        //Test if game is over
        private bool gameOver(player opponent)  //True Means game over
        {
            //Loop through all enemy ships
            foreach (Ship ship in opponent.ships)
            {
                //Test if ship is not a null reference
                if (ship != null)
                {
                    //End the game
                    return true;
                }
            }
            //Continue the game
            return false;
        }
    }
}