﻿using System;

namespace Battleship
{
    //Battleship Game Object
    internal class BattleshipGame
    {
        //Declare Variables
        public player player1;
        public player player2;

        //Run Main Game (Battleship)
        internal player Play(int current_player)
        {
            //Initialize Variables
            player winner;
            bool playing = true;

            //Initialize Players
            player1.init();
            player2.init();
            
            //Loop While Game is Ongoing
            while (playing)
            {
                //Run curren players name
                playing = (current_player == 0) ? player1.Turn(player2) : player2.Turn(player1);

                //Test if game is ongoing
                if (playing == false)
                {
                    //Delay
                    System.Threading.Thread.Sleep(2500);

                    //Get and return winner
                    winner = (current_player == 0) ? player1 : player2;
                    return winner;
                }

                //Switch to next player
                current_player = (current_player + 1) % 2;
            }
            return null;
        }
    }
}