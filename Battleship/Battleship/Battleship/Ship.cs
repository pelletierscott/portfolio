﻿using System;

namespace Battleship
{
    //Battleship Game Battleship Object
    internal class Ship
    {
        //Declare Variavles
        public string name;
        public int[] coords;

        //Ship Constructor
        public Ship(string name, int[] coords)
        {
            //Initialize Variables
            this.name = name;
            this.coords = coords;
        }

        //Test if Ship has been destroyed
        internal bool alive()
        {
            //Declare variable
            bool check = true;

            //Check each position of the ship
            foreach (int i in coords)
            {
                //Check if ship positions have been hit
                check = (i == -1) ? check : false;
            }

            //Return if ship is alive or not
            return check;
        }
    }
}