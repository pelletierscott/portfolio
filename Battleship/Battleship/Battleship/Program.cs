﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    class Program
    {
        //Program Main
        static void Main(string[] args)
        {
            //Initialize Variables
            bool replay = true;
            int current_player;
            BattleshipGame game = new BattleshipGame();

            //Loop while user is playing
            while (replay)
            {
                getGameType(game);                              //Get Game Type
                current_player = (flip()) ? 1 : 0;              //Flip for Initiative
                player winner = game.Play(current_player);      //Run Game
                displayWinner(game, winner);                    //Display Winner
                replay = (playAgain()) ? true : false;          //Go Again
            }
        }

        //Get the User(s) desired Match Type
        private static void getGameType(BattleshipGame game)
        {
            switch (vsType())
            {
                //Player vs Player
                case ("PvP"):
                    game.player1 = new player(0, "human");
                    game.player2 = new player(1, "human");
                    break;

                //Player vs A.I
                case ("PvAI"):
                    game.player1 = new player(0, "human");
                    game.player2 = new player(1, "machine");
                    break;

                //A.I vs A.I
                case ("AIvAI"):
                    game.player1 = new player(0, "machine");
                    game.player2 = new player(1, "machine");
                    break;

                //Error Occured - Exit Program
                default: Environment.Exit(0); break;
            }
        }

        //Cycleable Menu of Match Types
        private static string vsType()
        {
            //Initialize Variables
            string type = "";
            int selection = 0;
            bool flag = true;
            ConsoleKeyInfo key = new ConsoleKeyInfo((char)ConsoleKey.A, ConsoleKey.A, false, false, false); ;

            //Loop until a Match Type is Selected
            while (flag)
            {
                //Clear Console screen
                Console.Clear();

                //Store all Match Types
                string line1 = "Human vs. Human";
                string line2 = "Human vs. Machine";
                string line3 = "Machine vs. Machine";

                //Print Game Banner
                banner();

                //Add caveat to currently highlighted Match Type
                switch (selection)
                {
                    case (0): line1 = "> " + line1; type = "PvP"; break;
                    case (1): line2 = "> " + line2; type = "PvAI"; break;
                    case (2): line3 = "> " + line3; type = "AIvAI"; break;
                }

                //Write Menu Items
                Console.WriteLine("\t  " + line1);
                Console.WriteLine("\t  " + line2);
                Console.WriteLine("\t  " + line3);

                //Get User Input
                key = Console.ReadKey();

                //Interperate User Input
                switch (key.Key)
                {
                    case (ConsoleKey.UpArrow): selection--; break;
                    case (ConsoleKey.DownArrow): selection++; break;
                    case (ConsoleKey.Enter): flag = !flag; break;
                }

                //Rollover User Selection 
                selection = (selection + 3) % 3;
            }

            //Return Match Type
            return type;
        }

        //Flip a coin to determine player order
        private static bool flip()
        {
            //Clear Console screen
            Console.Clear();

            //Initialize Variables
            bool coin = false;
            System.Random rand = new System.Random();

            //Print Game Banner
            banner();

            //Print Information (Basic)
            Console.Write("\t  Flipping a coin");

            //Print Information (Teletype)
            for (int i = 0; i < 3; i++)
            { Console.Write("."); System.Threading.Thread.Sleep(1000); }

            //Produce Random Boolean Value
            if (rand.Next(2) == 0)
            { coin = true; Console.WriteLine("\n\t  Heads... Player 1 goes first\n"); }
            else
            { coin = false; Console.WriteLine("\n\t  Tails... Player 2 goes first\n"); }

            //Wait for User Input
            Console.WriteLine("\t  Player 1, Place Your Ship");
            Console.WriteLine("\t  Press any Key to Continue");
            Console.ReadKey();

            //Return results of coin toss
            return coin;
        }
 
        //Output winner of the game
        private static void displayWinner(BattleshipGame game, player winner)
        {
            //Clear Console Screen
            Console.Clear();

            //Print Game Banner
            banner();

            //Print Line based on Winning Player
            if (winner.number == 0)
            { Console.WriteLine("\n\t   Player 1 was Victorious !"); }
            else
            { Console.WriteLine("\n\t   Player 2 was Victorious !"); }

            //Print Winning Salute based on Match Type check
            if (game.player1.source == "human" && game.player2.source == "human")
            { Console.WriteLine("\n\t   You sank your enemies navy"); }
            else
            {
                //Test if winner was a human user
                if (winner.source == "humans")
                { Console.WriteLine("\n\t   You triumphed over the Machines"); }
                else
                { Console.WriteLine("\n\t   The Machine uprising has begun"); }
            }

            //Delay and pause game
            System.Threading.Thread.Sleep(5000);
            Console.ReadKey();
        }

        //Ask if User wants to Play Again
        private static bool playAgain()
        {
            //Clear Console Screen
            Console.Clear();

            //Pring Game Banner
            banner();

            //Print Information (Basic)
            Console.Write("\n\n\t   Would you like to play again (Y/N)");

            //Delay game
            System.Threading.Thread.Sleep(1000);

            //Get User Input
            ConsoleKeyInfo key = Console.ReadKey();

            //Inteperate User Input
            switch (key.Key)
            {
                case (ConsoleKey.Y):
                    return true;

                case (ConsoleKey.N):
                    return false;

                //Re-run Check if 
                default:
                    try
                    { return playAgain(); }
                    catch (StackOverflowException ex)
                    { return false; } 
            }
        }

        //Print ASCII Game Banner
        private static void banner()
        {
            Console.WriteLine(@"");   
            Console.WriteLine(@"    __/HHk_____");
            Console.WriteLine(@"    \_________/");
            Console.WriteLine(@"^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
            Console.WriteLine(@"");
            Console.WriteLine(@"	   ---BATTLESHIP---");
            Console.WriteLine(@"");
            Console.WriteLine(@"	By: Scott Pelletier");
            Console.WriteLine(@"	1.0 : January 7th 2019");
            Console.WriteLine(@"");
            Console.WriteLine(@"");
            Console.WriteLine(@"                               I");
            Console.WriteLine(@"                          XEEEEEED");
            Console.WriteLine(@"");
            Console.Write("\n");
            Console.WriteLine(@"____________________________________");
            Console.Write("\n");
        }
    }
}