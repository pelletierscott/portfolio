﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] strArr = new string[] { "alpha", "beta", "charlie", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?" };

            Teletype(strArr, 250, 0, 50, "\t");

            Console.ReadKey();
        }


        /*
        private static void teletype(string str)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, 200, 0, "\t", null, null);
        }

        private static void teletype(string str, int delay)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, delay, 0, "", null, null);
        }

        private static void teletype(string str, int delay, int spacing)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, delay, spacing, "\t", null, null);
        }

        private static void teletype(string str, int delay, int spacing, string indent)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, delay, spacing, indent, null, null);
        }

        private static void teletype(string str, int delay, int spacing, string indent, SystemSound typeSound)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, delay, spacing, indent, typeSound, null);
        }

        private static void teletype(string str, int delay, int spacing, string indent, SystemSound typeSound, SystemSound lineSound)
        {
            string[] strArr = new string[] { str };
            teletype(strArr, delay, spacing, indent, typeSound, lineSound);
        }

        private static void teletype(string[] strArr)
        {
            teletype(strArr, 200, 0, "\t", null, null);
        }

        private static void teletype(string[] strArr, int delay)
        {
            teletype(strArr, delay, 0, "\t", null, null);
        }

        private static void teletype(string[] strArr, int delay, int spacing)
        {
            teletype(strArr, delay, spacing, "\t", null, null);
        }

        private static void teletype(string[] strArr, int delay, int spacing, string indent)
        {
            teletype(strArr, delay, spacing, indent, null, null);
        }

        private static void teletype(string[] strArr, int delay, int spacing, string indent, SystemSound typeSound)
        {
            teletype(strArr, delay, spacing, indent, typeSound, null);
        }
        */





        //teletype(strArr, 200, 0, "\t", null, null);

        private static void Teletype(
            string[] strArr,
            int delay = 1000,
            int spacing = 0,
            int lineLength = 50,
            string indent = "\t",
            string typeSound = "beep",
            string lineSound = "beep")
        {

            int default_delay = 600;
            string[] strArrModified = StringLimiter(strArr, lineLength);

            foreach (string str in strArrModified)
            {

                Console.Write(indent);

                foreach (char c in str)
                {
                    if (Console.KeyAvailable)
                    {
                        ConsoleKeyInfo keyInfo = Console.ReadKey(true);

                        if (keyInfo.Key == ConsoleKey.Spacebar)
                        {
                            delay = 0;
                            default_delay = 0;
                        }
                    }

                    Console.Write(c);
                    if (typeSound != null) { PlayAudio(typeSound); }
                    System.Threading.Thread.Sleep(delay);


                }

                for (int i = 0; i <= spacing; i++)
                {
                    Console.Write("\n");
                    if (typeSound != null) { PlayAudio(lineSound); }
                    System.Threading.Thread.Sleep(Math.Max(delay, default_delay));
                }



              
            }
        }

        private static string[] StringLimiter(string[] strArr, int lineLength)
        {
            HashSet<string> lines = new HashSet<string>();
            string line = "";
            string word = "";

            foreach (string str in strArr)
            {
                foreach (char c in str)
                {
                    word += c;
                    if (c == ' ')
                    {
                        if ((line + word).Length <= lineLength)
                        {
                            line += word;
                            word = "";
                        }
                        else
                        {
                            lines.Add(line);
                            line = word;
                            word = "";
                        }
                    }
                    
                }
                line += word;
                lines.Add(line);
                word = "";
                line = "";
            }



            return lines.ToArray();
        }

        private static void PlayAudio(string sound)
        {
            switch(sound)
            {
                case ("beep"):
                    SystemSounds.Beep.Play();
                    break;

                default:
                    //Play Nothing
                    break;

            }
        }
    }
}
