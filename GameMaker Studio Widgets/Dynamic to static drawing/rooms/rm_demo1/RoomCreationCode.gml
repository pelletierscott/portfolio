///Instantiacte all versions of all NPC's


//Create object for resetting demonstration
instance_create_layer(0, 0, "instances", obj_reset);


//Create objects for rows data
var icon = instance_create_layer(50, 315+10, "instances", obj_r_c);
icon.image_angle = 0;
icon.text = "Row 1: This set of NPC's run all the logic for their death animations inside their own object";
var icon = instance_create_layer(50, 390+10, "instances", obj_r_c);
icon.image_angle = 0;
icon.text = "Row 2: This set of NPC's change to another object containing their logic for their death animation. Each NPC is the same but the object they change to different  ";
var icon = instance_create_layer(50, 465+10, "instances", obj_r_c);
icon.image_angle = 0;
icon.text = "Row 3: This set of NPC's pass their information to a control object that handles all the death animation logic for multiple NPCs";


//Create objects for column data
var icon = instance_create_layer(130, 250, "instances", obj_r_c);
icon.image_angle = 270;
icon.text = "Column 0: This animation simply stops the sprite's animation and leaves it running indefinitely.";
var icon = instance_create_layer(260, 250, "instances", obj_r_c);
icon.image_angle = 270;
icon.text = "Column 1: This animation stops the sprite's animation and draws a second sprite 'spr_ghost' bobbing above the NPC's corpse.";
var icon = instance_create_layer(390, 250, "instances", obj_r_c);
icon.image_angle = 270;
icon.text = "Column 2: This animation makes the NPC fade away before destroying it. The animation supports a delay period before the fade begins.";
var icon = instance_create_layer(520, 250, "instances", obj_r_c);
icon.image_angle = 270;
icon.text = "Column 3: This animation makes the NPC blink several times before destroying it. The animation supports a delay period before the blinking begins.";


//Create NPC with no death animation(s)
instance_create_layer(50, 240, "instances", obj_death_type_0);

//Create NPC's of type 1 (Internal logic for death animation) 
instance_create_layer(130, 315, "instances", obj_death_type_1_0);
instance_create_layer(260, 315, "instances", obj_death_type_1_1);
instance_create_layer(390, 315, "instances", obj_death_type_1_2);
instance_create_layer(520, 315, "instances", obj_death_type_1_3);

//Create NPC's of type 2 (Change object when dead)
	//Create NPC
var npc = instance_create_layer(130, 390, "instances", obj_death_type_2);
	//Assign death animation type 
npc.shell = obj_death_type_2_0;

var npc = instance_create_layer(260, 390, "instances", obj_death_type_2);
npc.shell = obj_death_type_2_1;

var npc = instance_create_layer(390, 390, "instances", obj_death_type_2);
npc.shell = obj_death_type_2_2;

var npc = instance_create_layer(520, 390, "instances", obj_death_type_2);
npc.shell = obj_death_type_2_3;

 
//Create NPC's of type 3 (Push dead NPC's to datastructure)
//Create NPC's
var npc = instance_create_layer(130, 465, "instances", obj_death_type_3_0);
var npc = instance_create_layer(260, 465, "instances", obj_death_type_3_1);
var npc = instance_create_layer(390, 465, "instances", obj_death_type_3_2);
var npc = instance_create_layer(520, 465, "instances", obj_death_type_3_3);
	

//Create object to hold deceased NPC data
instance_create_layer(0, 400, "instances", obj_type_3_0_controller);
instance_create_layer(0, 400, "instances", obj_type_3_1_controller);
instance_create_layer(0, 400, "instances", obj_type_3_2_controller);
instance_create_layer(0, 400, "instances", obj_type_3_3_controller);
