/// @desc Check life status


//Check if NPC is alive
if (life == false)
{
	//Reassign sprite from live NPC to dead NPC	
	sprite_index = spr_dying;
	
	//Check if dead NPC has finished and blink is set to off
	if (round(image_index) == image_number - 1)
	{
		instance_change(shell, true);	//Change NPC object
	}				
}

