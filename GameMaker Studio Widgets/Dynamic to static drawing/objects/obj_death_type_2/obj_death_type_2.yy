{
    "id": "24bfd1bb-7b84-4277-a8e1-866fb946b449",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_2",
    "eventList": [
        {
            "id": "43528a7e-aac9-41ca-975b-ae8f9b44d398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "24bfd1bb-7b84-4277-a8e1-866fb946b449"
        },
        {
            "id": "3bfe04ec-3b42-46ba-a2fc-224a6b509f05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "24bfd1bb-7b84-4277-a8e1-866fb946b449"
        },
        {
            "id": "6dab3b1e-fb0e-4c24-a933-f3834bd41c00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "24bfd1bb-7b84-4277-a8e1-866fb946b449"
        },
        {
            "id": "21608ae2-b030-4663-843b-811418d8251a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "24bfd1bb-7b84-4277-a8e1-866fb946b449"
        },
        {
            "id": "042918d3-673e-4908-a929-2e04e74e6bf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "24bfd1bb-7b84-4277-a8e1-866fb946b449"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}