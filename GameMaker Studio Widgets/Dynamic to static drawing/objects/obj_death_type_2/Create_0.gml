/// @desc Declare NPC variables


life = true;	//Set NPC as alive

shell = self;	//Set default shell 


//Declare NPC Text
//(For demoonstration purposes only)
text = "2: A nested if statement kills the NPC when 'life = false', stops the animation once 'spr_dying' reaches the final index of the animation, and changes itself to a specified object which performs the remaining death animation(s).";
