/// @desc Check life status


//Check if NPC is alive	
if (life == false)
{
	//Kill NPC
	sprite_index = spr_dying;
	
	//Check if NPC animation has ended
	if (round(image_index) == image_number - 1)
	{
		//Access controller
		with (obj_type_3_1_controller)
		{
			//Increment number of NPC's
			number_of_npcs += 1;
			
			//Pass NPC data to controller's data structure
			npc_array[number_of_npcs, 0] = other.x;
			npc_array[number_of_npcs, 1] = other.y;
			npc_array[number_of_npcs, 2] = 0;
				
			//Destroy NPC
			instance_destroy(other);
		}	
	}			
}
