{
    "id": "861cb95e-0062-40a2-bbb0-f03e2d61a78c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_3_1",
    "eventList": [
        {
            "id": "af6272b9-f3f9-4acb-a47d-9de088fb309e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "861cb95e-0062-40a2-bbb0-f03e2d61a78c"
        },
        {
            "id": "6abad509-b048-4579-b911-15b40e565a1b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "861cb95e-0062-40a2-bbb0-f03e2d61a78c"
        },
        {
            "id": "60fa5287-40e0-4cbf-8e12-1a688545f354",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "861cb95e-0062-40a2-bbb0-f03e2d61a78c"
        },
        {
            "id": "8bc45b59-975e-42ad-81db-25fc256755ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "861cb95e-0062-40a2-bbb0-f03e2d61a78c"
        },
        {
            "id": "4105252e-8d14-43aa-a5d9-a3a575d11625",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "861cb95e-0062-40a2-bbb0-f03e2d61a78c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}