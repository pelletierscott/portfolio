/// @desc Declare NPC variables


//Set NPC as alive
life = true;


//Declare NPC Text
//(For demoonstration purposes only)
text =	"3_1: A nested if statement kills the NPC when 'life = false', stops the animation once 'spr_dying' reaches the final index of the animation, and passes the NPC's position and a timer (default value of zero) to the control object's array.\n" + 
		"[For each entry in the array the controller draws the last index of 'spr_dying' at the corresponding coordinates in the array, increments a timer, and draws 'spr_ghost'.]\n" + 
		"[The controller uses an array because the NPCs is never removed. A different data structure should be used if NPC's of this type will be destroyed/removed from array.]";
