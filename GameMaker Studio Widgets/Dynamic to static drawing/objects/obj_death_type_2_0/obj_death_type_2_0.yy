{
    "id": "553933c6-21e3-4635-bb1d-8f1b2968dae6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_2_0",
    "eventList": [
        {
            "id": "1980d14a-901b-4961-864b-bda9209a46ec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "553933c6-21e3-4635-bb1d-8f1b2968dae6"
        },
        {
            "id": "bbdd69bb-c4d2-42cb-8894-9f6fea5c52bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "553933c6-21e3-4635-bb1d-8f1b2968dae6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
    "visible": true
}