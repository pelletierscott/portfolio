{
    "id": "979f2760-a245-4c1d-b13c-9cdde9fd877a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_1_2",
    "eventList": [
        {
            "id": "b09f573b-d81a-42b7-b000-f7315fa30107",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        },
        {
            "id": "5e22a5ce-4e0c-485f-a064-113c0c9dda0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        },
        {
            "id": "4515e35d-6a05-4162-81d3-dc3f447c625d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        },
        {
            "id": "ce44f26c-88e9-454a-ac97-4e90f5c815e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        },
        {
            "id": "bd0ee9b7-cf68-4d37-8a1d-71d9d6c1cd25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        },
        {
            "id": "b3f52af9-80d6-4dea-aeb5-8a193780744d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "979f2760-a245-4c1d-b13c-9cdde9fd877a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}