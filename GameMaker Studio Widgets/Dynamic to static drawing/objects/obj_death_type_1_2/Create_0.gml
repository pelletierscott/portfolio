/// @desc Declare NPC variables


life = true;		//Set NPC as alive

pace = 0.05;		//Change in alpha per update
rate = 5;			//The interval between alpha updates
delay = 20;			//The delay before NPC begins fading


//Declare NPC Text
//(For demoonstration purposes only)
text = "1_2: A nested if statement kills the NPC when 'life = false' and stops the animation once 'spr_dying' reaches the final index of the animation. Once these two criteria are met the alpha value of the sprite decrement each step until it reaches zero and the NPC is destroyed";
