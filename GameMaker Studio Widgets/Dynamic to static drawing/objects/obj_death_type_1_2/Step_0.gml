/// @desc Check life status


//Check if NPC is alive
if (life == false)
{
	//Reassign sprite from live NPC to dead NPC	
	sprite_index = spr_dying;
	
	//Check if dead NPC has finished its animation
	if (round(image_index) == image_number - 1)
	{
		image_speed = 0;		//Stop NPC animation
		alarm[0] = delay;		//Set delay to start fade out
		life = true;			//Revive NPC (for logic purposes)
	}			
}

//Check if NPC has vanished (alpha <= 0)
if (image_alpha <= 0)
{
	//Destroy NPC
	instance_destroy(self);
}