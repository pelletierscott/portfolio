/// @desc Check life status 


//Check if NPC is alive
if (life == false)
{
	//Kill NPC
	sprite_index = spr_dying;
	
	//Check if NPC animation has ended
	if (round(image_index) == image_number - 1)
	{
		//Pass NPC data to array 
		var temp_arr = [other.x, other.y, start_alpha, blink_rate, num_of_blinks];
		
		//Access controller
		with (obj_type_3_3_controller)
		{
			//Add NPC data to datastructure (as array)
			ds_list_add(npc_list, temp_arr);
		}
		
		//Destroy NPC
		instance_destroy(other);	
	}			
}

