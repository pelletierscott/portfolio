/// @desc Declare variables for NPC


//Set NPC is alive
life = true;

blink_rate = 0.1;			//Set blink rate
var delay = 1;				//Decalre delay before blinking
start_alpha = 1 + delay;	//Set initial alpha value
num_of_blinks = 10;			//Set number of blinks


//Declare NPC Text
//(For demoonstration purposes only)
text =	"3_3: A nested if statement kills the NPC when 'life = false', stops the animation once 'spr_dying' reaches the final index of the animation, and passes the NPC's position, and parameters for the blinking to a list data structure.\n" +  
		"[For each entry in the list the controller draws the last index of 'spr_dying' at the corresponding coordinates in the list, decrements the alpha, decrement the number of blinks, and checks if the entry is ready to be removed ('spr_dying alpha and number of blinks equal zero).]\n" + 
		"[The controller uses a ds_list to use the ds_list functions to add and remove entries as needed.]"
