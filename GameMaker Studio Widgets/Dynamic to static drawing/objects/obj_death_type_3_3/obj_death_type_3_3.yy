{
    "id": "b396495c-346a-42b7-b0b2-b241ead41956",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_3_3",
    "eventList": [
        {
            "id": "b43c63cb-0ed5-4550-a18e-42bb6d9e6504",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b396495c-346a-42b7-b0b2-b241ead41956"
        },
        {
            "id": "d5f92eb1-5adf-4ad1-bae9-b0b341824a8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b396495c-346a-42b7-b0b2-b241ead41956"
        },
        {
            "id": "2d8af72d-48bb-4e49-a7ec-049c597efcea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b396495c-346a-42b7-b0b2-b241ead41956"
        },
        {
            "id": "dbcc72ae-1469-4988-8466-065698129b00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "b396495c-346a-42b7-b0b2-b241ead41956"
        },
        {
            "id": "6286cf78-721b-4e78-bb62-373bb7bbb00b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "b396495c-346a-42b7-b0b2-b241ead41956"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}