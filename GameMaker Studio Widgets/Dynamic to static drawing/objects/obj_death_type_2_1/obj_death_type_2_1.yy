{
    "id": "22c26452-a5ee-4d10-8723-5e143beb9e55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_2_1",
    "eventList": [
        {
            "id": "3d7b0cfe-47c3-4705-a555-35a04b43fb31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22c26452-a5ee-4d10-8723-5e143beb9e55"
        },
        {
            "id": "78880128-c36e-4b6f-a10c-3a3737550814",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22c26452-a5ee-4d10-8723-5e143beb9e55"
        },
        {
            "id": "60190c77-a86c-4f6b-9a2c-3049507fa8b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "22c26452-a5ee-4d10-8723-5e143beb9e55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
    "visible": true
}