/// @desc Draw sprite and data


//Draw NPC
draw_self();

//Check if NPC is dead and animation had finished
if ((life == false) && (round(image_index) == image_number -1))
{
	//Draw NPC's ghost
	/*
		The period of a sin-wave is 2*pi so the range of the 
		function sin() only need to be 2*pi. The 'timer' only
		reaches from 0 to 2 and is then multiplied by pi
	*/
	draw_sprite(spr_ghost, 0, x, y + ghost_y + (sin(timer * pi) * amplitude));
}


//Declare NPC Text
//(For demoonstration purposes only)
scr_draw_metadata();