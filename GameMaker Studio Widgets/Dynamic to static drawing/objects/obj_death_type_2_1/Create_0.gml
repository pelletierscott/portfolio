/// @desc Declare NPC corpse variables


image_speed = 0;				//Stop animation
image_index = image_number -1;	//Set to end of animation

ghost_y = -15;					//Set the distance between NPC and NPC's ghost
timer = 0;						//Set timer to zero
frequency = 0.05;				//The rate the ghost oscillates at
amplitude = 5;					//The vertical range of the ghosts oscillations 


//Declare NPC Text
//(For demoonstration purposes only)
text = "2_1: The changed object sets the sprite speed, image index, and draws 'spr_ghost'.";
