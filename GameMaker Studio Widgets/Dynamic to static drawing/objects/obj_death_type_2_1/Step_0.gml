/// @desc Perform logic


//Check if NPC is alive
if (life == false)
{
	//Reassign sprite from live to dead NPC
	sprite_index = spr_dying;
	
	//Check if dead NPC has finished animation
	if (round(image_index) == image_number - 1)
	{
		//Stop NPC animation
		image_speed = 0;
	}				
}

//increment and modulate timer
timer = (timer + frequency) mod 2;
