{
    "id": "bce908d5-fb5d-423b-84e0-5f0fa217b3f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_r_c",
    "eventList": [
        {
            "id": "88bf5922-51c5-474d-a24f-5a45b56291d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bce908d5-fb5d-423b-84e0-5f0fa217b3f2"
        },
        {
            "id": "880ca4fd-14e5-4722-b8df-f98005b45a9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bce908d5-fb5d-423b-84e0-5f0fa217b3f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "81261eea-ab5e-4cdb-9057-8f7653289532",
    "visible": true
}