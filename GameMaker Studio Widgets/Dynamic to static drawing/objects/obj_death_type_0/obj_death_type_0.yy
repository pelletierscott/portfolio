{
    "id": "fc4917d8-f0cf-4267-a359-f08aaefee599",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_0",
    "eventList": [
        {
            "id": "8c52778a-caf5-404a-9161-59136ed26c67",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fc4917d8-f0cf-4267-a359-f08aaefee599"
        },
        {
            "id": "4472c075-ceec-4ab6-8b77-6cfb74bccde1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fc4917d8-f0cf-4267-a359-f08aaefee599"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}