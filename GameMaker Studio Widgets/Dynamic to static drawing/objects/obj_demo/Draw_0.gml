/// @desc Draw Demonstration instructions


//Draw graphics
draw_sprite_ext(spr_mouse,		0, 150, 450, 2, 2, 0, c_white, 1)
draw_sprite_ext(spr_spacebar,	0, room_width/2, 500, 2, 2, 0, c_white, 1)
draw_sprite_ext(spr_r_key,		0, room_width-150, 500, 2, 2, 0, c_white, 1)

//Set color and alignments
draw_set_color(c_white);
draw_set_halign(fa_left);

//Draw instructions
draw_text_ext_transformed(30, 20, 
"This demo contains different systems to manage Non-player characters (NPC) that have died.\n" +
"Each row uses a different way of managing the dead NPC\n" +
"Each column represents a different style of dead NPC\n" +
"Different styles have different advantages in terms of functionality, memory usage, or graphical load\n" +
"Move the mouse over the rows, columns, or NPC's to get more information.\n" +
"*Some NPC's will not display their information after they have died.",
15, 650, 1.5, 1.5, 0);

//Draw graphic texts
draw_text_ext_transformed(30, 600, "Left Click on an NPC to start it's death animation", 15, 130, 2, 2, 0);
draw_text_ext_transformed(380, 600, "Press Space bar to start all NPC's death animations ", 15, 130, 2, 2, 0);
draw_text_ext_transformed(740, 600, "Press 'R' Key to reset all NPC's", 15, 130, 2, 2, 0);

