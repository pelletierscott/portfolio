/// @desc Draw all NPC corpses


//Check if datastructure is empty
if (!ds_list_empty(npc_list))
{
	//Loop through all datastructure entries
	for (i = 0; i < ds_list_size(npc_list); i = i + 1)
	{
		//Access datastructure entry as an array
		var temp_arr = npc_list[| i];

		//Draw NPC corpse
		draw_sprite_ext(		
						spr_dead, 
						0, 
						temp_arr[i, 0],  
						temp_arr[i, 1], 
						1,
						1,
						0,
						c_white, 
						(temp_arr[i, 2]/10));
	}
}	

