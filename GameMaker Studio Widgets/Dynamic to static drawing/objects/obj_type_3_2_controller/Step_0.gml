/// @desc Perform logic


//Get size of the datastructure
var size = ds_list_size(npc_list);

//Loop through all entries of the datastructure
for (i = 0; i < ds_list_size(npc_list); i = i + 1)
{
	//Access datastructure entry as an array
	var temp_arr = npc_list[| i] ;
	
	//Decrement NPC alpha
	temp_arr[@ 2] -= rate;
	
	//Check if NPC is invisible
	if (temp_arr[2] <= 0)
	{
		//Remove NPC from datastructure
		ds_list_delete(npc_list, i);
	}
}	
