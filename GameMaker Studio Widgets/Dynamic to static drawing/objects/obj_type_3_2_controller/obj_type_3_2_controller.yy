{
    "id": "5ee93755-7256-4ac4-b99a-168ae86cb599",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_type_3_2_controller",
    "eventList": [
        {
            "id": "e8e46265-777c-4c06-a725-28c7504609d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5ee93755-7256-4ac4-b99a-168ae86cb599"
        },
        {
            "id": "b9396cc6-8f4d-4be1-abcf-5d8938eeac56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5ee93755-7256-4ac4-b99a-168ae86cb599"
        },
        {
            "id": "20bfd08a-0ef5-4661-af4e-d76134ae6194",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5ee93755-7256-4ac4-b99a-168ae86cb599"
        },
        {
            "id": "2f5b41ba-ade5-4514-a9c2-1a05bcbb01ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5ee93755-7256-4ac4-b99a-168ae86cb599"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}