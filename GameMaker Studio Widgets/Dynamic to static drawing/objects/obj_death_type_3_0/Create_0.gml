/// @desc Define the type of NPC death


//Test if NPC is alive
life = true;


//Declare NPC Text
//(For demoonstration purposes only)
text =	"3_0: A nested if statement kills the NPC when 'life = false', stops the animation once 'spr_dying' reaches the final index of the animation, and passes the NPC's position to the control object's array\n" +
		"[For each entry in the array the controller draws the last index of 'spr_dying' at the corresponding coordinates in the array.]\n" +
		"[The controller uses an array because the NPCs is never removed. A different data structure should be used if NPC's of this type will be destroyed/removed from array.]";



