/// @desc Check life status
	

//Check if NPC is alive
if (life == false)
{
	//Kill NPC
	sprite_index = spr_dying;
	
	//Check if NPC animation has ended
	if (round(image_index) == image_number - 1)
	{
		//Access controller
		with (obj_type_3_0_controller)
		{
			//Increment the number of NPC in the controller
			number_of_npcs += 1;
			
			//Pass data to the controller's datastructure
			npc_array[number_of_npcs, 0] = other.x;
			npc_array[number_of_npcs, 1] = other.y;
			
			//Destroy the NPC object
			instance_destroy(other);
		}
	}			
}
