{
    "id": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_3_0",
    "eventList": [
        {
            "id": "53c729fd-4e97-4337-867a-329d92199711",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f"
        },
        {
            "id": "b705d557-4743-4a67-9f0d-6ce993ce53fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f"
        },
        {
            "id": "e0b7c22a-f137-4ec1-82e8-ab3099b20779",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f"
        },
        {
            "id": "72f41161-9473-4fbb-8174-c41ecc0a3324",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f"
        },
        {
            "id": "574b079c-6d5f-4f1b-88ca-0d35db1d224f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "f7268f2e-9f4a-4d16-8db7-cd4eb3bd176f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}