/// @desc Check life status


//Check if NPC is alive
if (life == false)
{
	//Reassign sprite from live NPC to dead NPC	
	sprite_index = spr_dying;
	
	//Check if dead NPC has finished and blink is set to off
	if ((round(image_index) == image_number - 1) && (blink == false))
	{
		image_speed = 0;		//Stop NPC animation
		blink = true;			//Set blink to on
	}			
}

//Check if NPC is blinking
if (blink == true)
{
	npc_alpha += blink_rate;	//Adjust alpha value
	
	//Check if NPC has reached last blink
	if (num_of_blinks <= 0)
	{
		instance_destroy(self);	//Destroy NPC's corpse
	}

	//Check if NPC is fully visible and alpha change is positive
	/*
		This is structured this way to allow for a delay 
		before the NPC starts blinking.		
	*/
	if ((npc_alpha >= 1) && (blink_rate >= 0))
	{
		blink_rate *= -1;		//Invert blink alpha change
	}

	//Check if NPC is fully INvisible
	if (npc_alpha <= 0)
	{
		blink_rate *= -1;		//Invert blink alpha change
		num_of_blinks -= 1;		//Deincrement number of blinks remaining
	}
}