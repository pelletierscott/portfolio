{
    "id": "a360ed5c-449b-45cb-906c-0e91d4a2f570",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_1_3",
    "eventList": [
        {
            "id": "8ca2c724-e424-4c05-bab0-18a8b6f35b52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a360ed5c-449b-45cb-906c-0e91d4a2f570"
        },
        {
            "id": "9efba69f-3059-4e1c-94b7-a8400eb4ce2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a360ed5c-449b-45cb-906c-0e91d4a2f570"
        },
        {
            "id": "51dfa34a-d845-45f0-a198-8ada6d496d83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a360ed5c-449b-45cb-906c-0e91d4a2f570"
        },
        {
            "id": "9c958319-870d-44fc-a124-a4d67d2bde83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "a360ed5c-449b-45cb-906c-0e91d4a2f570"
        },
        {
            "id": "56987e22-60ee-46bc-ba24-fae254e79aba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "a360ed5c-449b-45cb-906c-0e91d4a2f570"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}