/// @desc Declare NPC variables


life = true;		//Set NPC as alive

blink = false;		//Set blinking to off
blink_rate = -0.1;	//The change in alpha per step

/*
	A delay can be inserted into the NPC before it starts 
	blinking. By making 'npc_alpha' greater than 1. The NPC
	won't begin to blink until 'npc_alpha' has been reduced
	to 1 or less
		delay(value)	=	delay (in steps)	* -blink_rate
		1				=	10					* -0.1
		10				=	100					* -0.1
*/
npc_alpha = 2;		//The start alpha value (1 + delay (value))
num_of_blinks = 10;	//Number of blinks to execute


//Declare NPC Text
//(For demoonstration purposes only)
text = "1_3: A nested if statement kills the NPC when 'life = false' and stops the animation once 'spr_dying' reaches the final index of the animation. Once these two criteria are met the alpha value and the number of remaining blinks are decremented until both are zero and the NPC is destroyed.";

