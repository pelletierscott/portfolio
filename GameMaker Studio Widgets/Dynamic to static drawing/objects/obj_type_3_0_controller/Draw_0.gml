/// @desc Draw NPC corpses


//Test if data structure exists and contains something to draw
if ((npc_array != undefined) && (array_height_2d(npc_array) > 0))
{
	//Loop through all entries(disregard line 1 which contains headers)
	for (i = 1; i < array_height_2d(npc_array); i = i + 1) 
	{
		//draw the NPC corpse at the stored position
		/*
			The functionality of the controller can be expanded
			by expanding the array to include more data about
			the NPC's.		
		*/
		draw_sprite(spr_dead, 0, npc_array[i, 0],  npc_array[i, 1]);
	}
}	
