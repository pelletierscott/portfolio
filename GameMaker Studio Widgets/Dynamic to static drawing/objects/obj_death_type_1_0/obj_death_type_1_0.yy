{
    "id": "d4d0d419-07fc-411a-a669-06069eccf7af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_1_0",
    "eventList": [
        {
            "id": "341b74a1-0bff-4b66-b008-3344a3e7a57e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4d0d419-07fc-411a-a669-06069eccf7af"
        },
        {
            "id": "ad2157de-254e-445f-b2fc-2afb09254c87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4d0d419-07fc-411a-a669-06069eccf7af"
        },
        {
            "id": "36be590e-a7ef-4d08-9b8a-d8bc2dba4d55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d4d0d419-07fc-411a-a669-06069eccf7af"
        },
        {
            "id": "30a4ebeb-0e84-4666-908f-4861d0d6af4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "d4d0d419-07fc-411a-a669-06069eccf7af"
        },
        {
            "id": "a86e8a1d-3c50-4f5c-a349-043ca1672812",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "d4d0d419-07fc-411a-a669-06069eccf7af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}