/// @desc Declare NPC variables


life = true;		//Set NPC as alive

//Declare NPC Text
//(For demoonstration purposes only)
text = "1_0: A nested if statement kills the NPC when 'life = false' and stops the animation once 'spr_dying' reaches the final index of the animation.";
