{
    "id": "f6b531d5-be8f-4141-abba-88fe0e039493",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_type_3_1_controller",
    "eventList": [
        {
            "id": "1041ea36-6263-419b-bbf3-4eddaae5f4c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f6b531d5-be8f-4141-abba-88fe0e039493"
        },
        {
            "id": "6b30cc23-27bf-4ea9-b33e-adbc9a433c59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f6b531d5-be8f-4141-abba-88fe0e039493"
        },
        {
            "id": "79b8790d-44fc-4ed2-a33c-fc1f738c3239",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f6b531d5-be8f-4141-abba-88fe0e039493"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}