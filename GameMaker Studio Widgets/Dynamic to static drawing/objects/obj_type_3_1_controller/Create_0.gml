/// @desc Devlare variables for controller


//Set number of NPC 
number_of_npcs = 0;

//Declare and set headers to datastructure
npc_array[0, 0] = "x_coord";
npc_array[0, 1] = "y_coord";
npc_array[0, 2] = "timer";


ghost_y = -15;					//Set the distance between NPC and NPC's ghost
frequency = 0.05;				//The rate the ghost oscillates at
amplitude = 5;					//The vertical range of the ghosts oscillations 

