/// @desc Draw all NPC corpses and ghosts


//Check if datastructure exists and contains at least one entry (beyond the headers)
if ((npc_array != undefined) && (array_height_2d(npc_array) > 0))
{
	//Loop through all entries of the datastructure
	for (i = 1; i < array_height_2d(npc_array); i = i + 1) 
	{
		//Draw the NPC's corpse
		draw_sprite(spr_dead, 0, npc_array[i, 0],  npc_array[i, 1]);
		
		//Draw the NPC's ghost
		draw_sprite(spr_ghost, 0, npc_array[i, 0],  npc_array[i, 1] + ghost_y + (sin(npc_array[i, 2] * pi) * amplitude));
	}
}	

