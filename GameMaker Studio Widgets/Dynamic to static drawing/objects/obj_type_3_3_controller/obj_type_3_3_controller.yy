{
    "id": "7eff8b15-520f-47c6-8bd9-db7e023f3b46",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_type_3_3_controller",
    "eventList": [
        {
            "id": "0ae56ccd-111b-48cb-8aaf-8f771d25035e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7eff8b15-520f-47c6-8bd9-db7e023f3b46"
        },
        {
            "id": "c108e73d-b74c-47c5-9b3d-057b5dfadadc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7eff8b15-520f-47c6-8bd9-db7e023f3b46"
        },
        {
            "id": "efd8f7e7-556f-434e-8acf-3b20b8824294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7eff8b15-520f-47c6-8bd9-db7e023f3b46"
        },
        {
            "id": "b64f9d57-5971-4eec-8e17-d05094e7fb6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7eff8b15-520f-47c6-8bd9-db7e023f3b46"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}