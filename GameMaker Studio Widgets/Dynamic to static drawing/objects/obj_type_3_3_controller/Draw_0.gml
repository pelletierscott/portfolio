/// @desc Draw NPC corpse


//Check datastructure exists
if (!ds_list_empty(npc_list))
{
	//Loop though all entries of the datastructure
	for (i = 0; i < ds_list_size(npc_list); i = i + 1)
	{
		//Access datastructure entry as an array
		var temp_arr = npc_list[| i];
		
		//Draw NPC corpse	
		draw_sprite_ext(
						spr_dead, 
						0, 
						temp_arr[@ 0],
						temp_arr[@ 1],
						1,
						1,
						0,
						c_white,
						temp_arr[@ 2]);
	}
}
