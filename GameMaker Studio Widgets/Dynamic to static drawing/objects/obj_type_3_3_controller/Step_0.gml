/// @desc perform logic


//Get size of the datastructure
var size = ds_list_size(npc_list);

//Loop through all entries of the datastructure
for (i = 0; i < ds_list_size(npc_list); i = i + 1)
{
	//Access datastructure entry as an array
	var temp_arr = npc_list[| i] ;
	
	//Decrement alpha value with blink rate
	temp_arr[@ 2] -= temp_arr[@ 3];
	
	//Check if NPC is invisible
	if (temp_arr[@ 2] <= 0)
	{
		temp_arr[@ 4] -= 1;		//Decrement number of blinks remaining
		temp_arr[@ 3] *= -1;	//Invert blink rate
	}
	
	//Check if NPC is fully visible and alpha change is positive
	/*
		This is structured this way to allow for a delay 
		before the NPC starts blinking.		
	*/
	if  ((temp_arr[@ 2] >= 1) && (temp_arr[@ 3] < 0))
	{
		temp_arr[@ 3] *= -1;	//Invert blink rate
	}
	
	//Check if blinking has ended 
	if (temp_arr[4] <= 0)
	{
		//Remove NPC from datastructures
		ds_list_delete(npc_list, i);
	}
}
