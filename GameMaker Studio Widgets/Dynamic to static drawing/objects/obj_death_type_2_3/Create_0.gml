/// @desc Declare NPC corpse variables


image_speed = 0;				//Stop NPC animation
image_index = image_number -1;	//Set to end of animation

blink_rate = -0.1;				//The change in alpha per step
num_of_blinks = 10;				//Set number of blinks
/*
	A delay can be inserted into the NPC before it starts 
	blinking. By making 'npc_alpha' greater than 1. The NPC
	won't begin to blink until 'npc_alpha' has been reduced
	to 1 or less
		delay(value)	=	delay (in steps)	* -blink_rate
		1				=	10					* -0.1
		10				=	100					* -0.1
*/
npc_alpha = 2;			//The start alpha value (1 + delay (value))				


//Declare NPC Text
//(For demoonstration purposes only)
text = "2_3: The changed object sets the sprite speed, image index, and decrements the alpha value and the number of remaining blinks until both are zero and the NPC is destroyed.";

