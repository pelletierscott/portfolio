{
    "id": "7339cd6d-1e51-4213-8671-2ae4419b2100",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_2_3",
    "eventList": [
        {
            "id": "61057da0-6b5a-4ba5-aa20-3c9eae3894e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7339cd6d-1e51-4213-8671-2ae4419b2100"
        },
        {
            "id": "959f94e1-a500-4f4d-833d-05d3f6265867",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7339cd6d-1e51-4213-8671-2ae4419b2100"
        },
        {
            "id": "e22837f9-bad6-4548-9a10-17196e25006b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7339cd6d-1e51-4213-8671-2ae4419b2100"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
    "visible": true
}