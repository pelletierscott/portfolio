/// @desc Perform Logic
	
	
//Adjust NPC alpha
npc_alpha += blink_rate;

//Check if blinking has finished
if (num_of_blinks <= 0)
{
	instance_destroy(self);		//Destroy the NPC's
}

//Check if NPC is fully visible and alpha change is positive
/*
	This is structured this way to allow for a delay 
	before the NPC starts blinking.		
*/
if ((npc_alpha >= 1) && (blink_rate >= 0))
{
	blink_rate *= -1;			//Invert blink rate
}

//Check if NPC is invisible
if (npc_alpha <= 0)				
{
	blink_rate *= -1;			//Invert blink rate
	num_of_blinks -= 1;			//Decrement number of blinks remaining
}

