/// @desc Declare NPC variables


image_speed = 0;				//Stop NPC animation
image_index = image_number -1;	//Set to end of animation

pace = 0.05;					//Change in alpha per update
rate = 5;						//The interval between alpha updates
delay = 20;						//The delay before NPC begins fading

alarm_set(0, delay);			//Set alarm to delay


//Declare NPC Text
//(For demoonstration purposes only)
text = "2_2: The changed object sets the sprite speed, image index, and decrements the alpha value until it reaches zero and the NPC is destroyed.";
