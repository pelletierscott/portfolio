{
    "id": "7233c340-45f4-489b-8a55-d1f632b133e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_2_2",
    "eventList": [
        {
            "id": "9eb8aa08-7b02-439d-bcce-26e816c015a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7233c340-45f4-489b-8a55-d1f632b133e8"
        },
        {
            "id": "63c83b79-efc2-475d-9316-e0287b3a6675",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7233c340-45f4-489b-8a55-d1f632b133e8"
        },
        {
            "id": "8f5b65f9-1556-4a7d-9ae1-afe474931098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7233c340-45f4-489b-8a55-d1f632b133e8"
        },
        {
            "id": "6cbb7149-54b0-45b9-88fe-8243ffee5b69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7233c340-45f4-489b-8a55-d1f632b133e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
    "visible": true
}