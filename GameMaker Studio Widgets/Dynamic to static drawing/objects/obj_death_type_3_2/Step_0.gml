/// @desc Check life status


//Check if NPC is Alive
if (life == false)
{
	//Kill NPC
	sprite_index = spr_dying;
	
	//Check if NPC animation has ended
	if (round(image_index) == image_number - 1)
	{
		//Create temporary array to hold NPC's data
		var temp_arr = [other.x, other.y, delay];
		
		//Access controller
		with (obj_type_3_2_controller)
		{
			//Pass data (in array form) to controller's datastructure
			ds_list_add(npc_list, temp_arr);
		}
		
		//Destroy NPC
		instance_destroy(other);			
	}			
}

