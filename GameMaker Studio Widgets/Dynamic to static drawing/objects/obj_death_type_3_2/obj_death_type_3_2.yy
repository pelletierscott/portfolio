{
    "id": "e10388ab-9e3f-400f-9c44-83d5be1515f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_3_2",
    "eventList": [
        {
            "id": "fac1097c-cbf9-4e9b-9489-8735eaa4edea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e10388ab-9e3f-400f-9c44-83d5be1515f2"
        },
        {
            "id": "b2501ec2-8817-4c3c-9739-f5caf57fe3cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e10388ab-9e3f-400f-9c44-83d5be1515f2"
        },
        {
            "id": "510b98b6-340e-4f9a-ae0b-4a82403f14d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e10388ab-9e3f-400f-9c44-83d5be1515f2"
        },
        {
            "id": "d712ad54-d452-4978-a599-18765b26900a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "e10388ab-9e3f-400f-9c44-83d5be1515f2"
        },
        {
            "id": "339f7750-9db7-42bd-a528-a841b7b099c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "e10388ab-9e3f-400f-9c44-83d5be1515f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}