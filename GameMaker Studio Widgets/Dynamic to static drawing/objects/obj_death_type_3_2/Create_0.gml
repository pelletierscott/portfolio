/// @desc Declare variables for NPC


//Set NPC as alive
life = true;

//Set delay for fade
delay = 20;


//Declare NPC Text
//(For demoonstration purposes only)
text =	"3_2: A nested if statement kills the NPC when 'life = false', stops the animation once 'spr_dying' reaches the final index of the animation, and passes the NPC's position, and parameters for the fade out to a list data structure.\n" + 
		"[For each entry in the list the controller draws the last index of 'spr_dying' at the corresponding coordinates in the list, decrements the alpha and checks if the entry is ready to be removed ('spr_dying alpha equal zero).]\n" + 
		"[The controller uses a ds_list to use the ds_list functions to add and remove entries as needed.]";

