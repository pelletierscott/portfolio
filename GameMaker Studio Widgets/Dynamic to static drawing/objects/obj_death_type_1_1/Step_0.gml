/// @desc Check life status


//Check if NPC is alive
if (life == false)
{
	//Reassign sprite from live NPC to dead NPC	
	sprite_index = spr_dying;
	
	//Check if dead NPC has finished its animation
	if (round(image_index) == image_number - 1)
	{
		//Stop NPC animation
		image_speed = 0;
	}				
}

//Recalculate timer
timer = (timer + frequency) mod 2;
	
	