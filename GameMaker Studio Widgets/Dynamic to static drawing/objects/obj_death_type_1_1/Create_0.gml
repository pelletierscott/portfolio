/// @desc Declare NPC variables


life = true;		//Set NPC as alive

ghost_y = -15;		//Set the distance between NPC and NPC's ghost
timer = 0;			//Set timer to zero
frequency = 0.05;	//The rate the ghost oscillates at
amplitude = 5;		//The vertical range of the ghosts oscillations 


//Declare NPC Text
//(For demoonstration purposes only)
text = "1_1: A nested if statement kills the NPC when 'life = false' and stops the animation once 'spr_dying' reaches the final index of the animation. Once these two criteria are met the object also draws 'spr_ghost'.";
