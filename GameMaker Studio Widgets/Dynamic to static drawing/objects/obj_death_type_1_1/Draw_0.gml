/// @desc Draw sprite and data


//Draw NPC
draw_self();

//Check if NPC is dead and reached end of animation
if ((life == false) && (round(image_index) == image_number -1))
{
	//Draw NPC's ghost 
	draw_sprite(spr_ghost, 0, x, y + ghost_y + (sin(timer * pi) * amplitude));
}

//Draw metadata
scr_draw_metadata();