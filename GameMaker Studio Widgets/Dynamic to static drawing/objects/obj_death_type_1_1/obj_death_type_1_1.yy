{
    "id": "b51e184d-7a2e-4912-ad33-44c28b152e3f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_death_type_1_1",
    "eventList": [
        {
            "id": "76ba1516-b112-454f-a508-a108fc529636",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b51e184d-7a2e-4912-ad33-44c28b152e3f"
        },
        {
            "id": "4c7a1a74-6775-4a56-a753-f4ed465db90b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b51e184d-7a2e-4912-ad33-44c28b152e3f"
        },
        {
            "id": "5bb8d02d-37db-4a1a-9515-d0daa5b1671f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b51e184d-7a2e-4912-ad33-44c28b152e3f"
        },
        {
            "id": "5371d6b3-da27-4214-9427-0f0bb59a5420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "b51e184d-7a2e-4912-ad33-44c28b152e3f"
        },
        {
            "id": "7225c0f8-f065-471e-a5f6-886d2752eae6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "b51e184d-7a2e-4912-ad33-44c28b152e3f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "visible": true
}