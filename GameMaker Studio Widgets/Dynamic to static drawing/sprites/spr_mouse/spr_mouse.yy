{
    "id": "651879ab-64af-439e-a0bd-66f1db50484c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 0,
    "bbox_right": 58,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7722379b-2caa-487c-9da1-e0a7316469cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "651879ab-64af-439e-a0bd-66f1db50484c",
            "compositeImage": {
                "id": "c7ecd504-dddb-44c8-a469-655e8b826e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7722379b-2caa-487c-9da1-e0a7316469cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1dcf5bd-4527-4a0b-b688-24a404a7641c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7722379b-2caa-487c-9da1-e0a7316469cb",
                    "LayerId": "2a332035-3d1e-4b0a-b7d0-d76291d13d77"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 120,
    "layers": [
        {
            "id": "2a332035-3d1e-4b0a-b7d0-d76291d13d77",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "651879ab-64af-439e-a0bd-66f1db50484c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 59,
    "xorig": 29,
    "yorig": 60
}