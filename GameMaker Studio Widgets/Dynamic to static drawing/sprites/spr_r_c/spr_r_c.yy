{
    "id": "81261eea-ab5e-4cdb-9057-8f7653289532",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_r_c",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 34,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0907d693-feeb-46e0-8df0-5eed24e799d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "81261eea-ab5e-4cdb-9057-8f7653289532",
            "compositeImage": {
                "id": "6a92132b-f9c8-4752-a59f-675e37789fb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0907d693-feeb-46e0-8df0-5eed24e799d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfdde8be-af05-416b-a20d-4329cf073feb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0907d693-feeb-46e0-8df0-5eed24e799d4",
                    "LayerId": "ad93d3ca-c1ce-45b9-bc86-59a31ccc81f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "ad93d3ca-c1ce-45b9-bc86-59a31ccc81f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "81261eea-ab5e-4cdb-9057-8f7653289532",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 17
}