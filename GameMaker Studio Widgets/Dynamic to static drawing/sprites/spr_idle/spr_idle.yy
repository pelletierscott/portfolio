{
    "id": "547895e7-e3c8-4776-b4e2-85f948adf003",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4049841c-bbca-4b75-bbd3-ce503f00403b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
            "compositeImage": {
                "id": "4e9ff665-aa63-494c-9a65-fac31df46572",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4049841c-bbca-4b75-bbd3-ce503f00403b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c566101f-ca43-48f8-ac9f-6cb65bc07003",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4049841c-bbca-4b75-bbd3-ce503f00403b",
                    "LayerId": "ba0c36a6-bf20-454a-8499-8f1191a86ed0"
                }
            ]
        },
        {
            "id": "875ea4ea-7f31-4d4f-ad3b-2eb435c2813d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
            "compositeImage": {
                "id": "f704bffb-f944-4e32-9972-6947281e46b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875ea4ea-7f31-4d4f-ad3b-2eb435c2813d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "610cdd6a-538d-4513-b707-409ed7fed588",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875ea4ea-7f31-4d4f-ad3b-2eb435c2813d",
                    "LayerId": "ba0c36a6-bf20-454a-8499-8f1191a86ed0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "ba0c36a6-bf20-454a-8499-8f1191a86ed0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "547895e7-e3c8-4776-b4e2-85f948adf003",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 33,
    "xorig": 16,
    "yorig": 9
}