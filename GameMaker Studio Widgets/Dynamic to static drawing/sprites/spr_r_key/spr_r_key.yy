{
    "id": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_r_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4f43362-fe20-4ff9-bae7-bda3ba755231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
            "compositeImage": {
                "id": "a3e74b1d-cfcc-4a08-80d7-cf063b38d621",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4f43362-fe20-4ff9-bae7-bda3ba755231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c320019f-90df-498e-ba3b-a4d95c24576e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4f43362-fe20-4ff9-bae7-bda3ba755231",
                    "LayerId": "5df23bec-c741-49e8-b05c-ceae1f6d12e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "5df23bec-c741-49e8-b05c-ceae1f6d12e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 13
}