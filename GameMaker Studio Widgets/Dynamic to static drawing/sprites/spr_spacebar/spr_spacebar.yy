{
    "id": "5673098b-893a-4c5c-bc82-a85e5e13f211",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spacebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 168,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08745c3e-f060-4425-ba9c-dc68c84e1d47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5673098b-893a-4c5c-bc82-a85e5e13f211",
            "compositeImage": {
                "id": "12d427ba-b24c-4cb2-a804-87ad275a9667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08745c3e-f060-4425-ba9c-dc68c84e1d47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b22b05-43b2-4396-81e7-3fcf5aca3db9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08745c3e-f060-4425-ba9c-dc68c84e1d47",
                    "LayerId": "31b19bb8-ab78-432a-a4c4-35ddcf0aae3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "31b19bb8-ab78-432a-a4c4-35ddcf0aae3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5673098b-893a-4c5c-bc82-a85e5e13f211",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 169,
    "xorig": 84,
    "yorig": 13
}