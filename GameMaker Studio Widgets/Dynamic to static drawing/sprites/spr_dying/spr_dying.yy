{
    "id": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 11,
    "bbox_right": 43,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2557c765-378a-4936-8181-7a68e2105cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "a262ef83-382e-4087-9163-8f8016dbcb1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2557c765-378a-4936-8181-7a68e2105cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1bd5467-bf5c-42c2-8c9d-aafa3d5b3dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2557c765-378a-4936-8181-7a68e2105cfa",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "47e76244-f7ad-4b10-9992-cd91cecab2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "1aa9574d-683d-40dc-bd14-957cfdf24302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e76244-f7ad-4b10-9992-cd91cecab2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787564b6-20e1-4774-9ff9-2cef635f1807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e76244-f7ad-4b10-9992-cd91cecab2ac",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "5d39cbc8-a682-4bc3-b13b-e3c983215244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "930bc724-6ba4-4b19-8cf0-b8c277c11fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d39cbc8-a682-4bc3-b13b-e3c983215244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daf04352-f8e8-4c61-9626-fc705036c6af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d39cbc8-a682-4bc3-b13b-e3c983215244",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "7a7b722e-89de-42ac-9fc6-6774e823af55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "35c188a6-8fcd-42ab-b355-44e712840b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a7b722e-89de-42ac-9fc6-6774e823af55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30bbad22-d0fa-4364-941d-976ae836ae9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a7b722e-89de-42ac-9fc6-6774e823af55",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "db565ba9-fcbe-4540-997d-3acdbb440942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "53f0c48e-11c4-4f4d-9578-6e710047c4c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db565ba9-fcbe-4540-997d-3acdbb440942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5378cc5f-b453-4f48-890f-f7c9f7a60bf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db565ba9-fcbe-4540-997d-3acdbb440942",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "aa2f120a-42ae-4780-9d54-780dab6e814b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "ad24cdcd-b03e-431b-9695-0c761b509733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa2f120a-42ae-4780-9d54-780dab6e814b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37715f1f-a06a-43dc-a9f9-5b00ba53d683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa2f120a-42ae-4780-9d54-780dab6e814b",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "2e44e71f-456c-4ac6-9e4b-4ec30535565c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "a6f28c22-ea15-486e-a9c7-e0052f178796",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e44e71f-456c-4ac6-9e4b-4ec30535565c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df0f4153-be7c-4bed-a1dd-15f7eae7bce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e44e71f-456c-4ac6-9e4b-4ec30535565c",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "08f75c2c-f343-4595-aa25-d39675e3c4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "59c90513-26ea-4791-9219-e2acb7e07fc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f75c2c-f343-4595-aa25-d39675e3c4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a48cfe85-9605-495f-b0fe-78145b87c669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f75c2c-f343-4595-aa25-d39675e3c4a2",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "206fc7e1-aecc-4b0e-a398-100fdf5e37b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "08f68354-d8bf-41ea-85ae-35838b330146",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "206fc7e1-aecc-4b0e-a398-100fdf5e37b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ced6008e-3544-4cd3-a132-0bf3d6fb965a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "206fc7e1-aecc-4b0e-a398-100fdf5e37b6",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        },
        {
            "id": "d368741e-ef20-4414-acfe-1ad78ac7aa18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "compositeImage": {
                "id": "8a591e28-b0eb-424f-a23c-70d79d42e5ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d368741e-ef20-4414-acfe-1ad78ac7aa18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9a431ed-f4d6-426d-a1fc-5e3e9da5c940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d368741e-ef20-4414-acfe-1ad78ac7aa18",
                    "LayerId": "5877aee0-b212-4eb0-99b9-5ef546c1cd02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "5877aee0-b212-4eb0-99b9-5ef546c1cd02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d5a5f2f1-8399-42cf-839f-d3b3fbebd465",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 21,
    "yorig": 9
}