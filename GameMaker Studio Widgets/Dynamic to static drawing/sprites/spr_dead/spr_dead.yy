{
    "id": "f97130f3-ae3d-49bb-a5ba-441d364f9454",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 11,
    "bbox_right": 43,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6258df3c-1d82-4d30-83eb-a974a7d05b5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f97130f3-ae3d-49bb-a5ba-441d364f9454",
            "compositeImage": {
                "id": "a04772ee-cadd-409a-942f-f5810f9a2e60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6258df3c-1d82-4d30-83eb-a974a7d05b5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6179f508-a04e-4d38-966f-9692b2501279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6258df3c-1d82-4d30-83eb-a974a7d05b5e",
                    "LayerId": "8775bb11-b318-4fbd-ae19-32fcd2398461"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "8775bb11-b318-4fbd-ae19-32fcd2398461",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f97130f3-ae3d-49bb-a5ba-441d364f9454",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 21,
    "yorig": 9
}