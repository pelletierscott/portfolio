{
    "id": "a56c57df-46e2-4f69-99c4-e428e5dd045f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ghost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34461a5b-fcc1-4dbd-84fe-bf387696e879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a56c57df-46e2-4f69-99c4-e428e5dd045f",
            "compositeImage": {
                "id": "de7c5b6c-521d-490f-bf1c-50fa701fd001",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34461a5b-fcc1-4dbd-84fe-bf387696e879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81989996-d78a-4c53-8fb0-47c7b4b548fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34461a5b-fcc1-4dbd-84fe-bf387696e879",
                    "LayerId": "e628ce7f-9094-460e-a424-714d7031d8f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "e628ce7f-9094-460e-a424-714d7031d8f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a56c57df-46e2-4f69-99c4-e428e5dd045f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 15,
    "xorig": 7,
    "yorig": 23
}