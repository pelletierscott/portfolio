///scr_colorrange(value, scale)
///@function scr_colorrange(value, scale)
///@param {real} value The value which to turn into a color.
///@param {real} scale The type of color scale to use.


/*
	Made by: Scott Pelletier
	August 02nd 2018
*/

//Decalre variables
var value = argument0;		//The value to evaluate into a color
var select = argument1;		//The color scale to utilize
var red = 0;
var green = 0;
var blue = 0;
var center = pi/2;			//Declare the center of the color scale


//Check if the calling object has 
if (	variable_instance_exists(self, "colorrange_min") &&
		variable_instance_exists(self, "colorrange_mid") &&
		variable_instance_exists(self, "colorrange_max"))
{
	//IF Object has custom scale variables add them to the script
	var cr_min = colorrange_min;
	var cr_mid = colorrange_mid;
	var cr_max = colorrange_max;
}
else
{
	//ELSE use scripts default values
	var cr_min = 0;
	var cr_mid = 100;
	var cr_max = 200;
}


//Calculate the scale of the lower range of the color scale (min - to - mid)
var value_scaled = ((center / (cr_mid - cr_min)) * (value - cr_min));

//Check if value is in the upper range (mid - to - max)
if (value > cr_mid)
{
	//Calculate the scale of the upper range (mid - to - max) 	
	value_scaled = ((center / (cr_max - cr_mid)) * (value - cr_mid)) + center;
}


//Pass the value to the selected scale
switch (select)
{
	#region Color Scale Version 0
	case (0):
	{
		//RED
		red =		0.1222 * power(value_scaled, 6) - 
					0.7201 * power(value_scaled, 5) + 
					1.6029 * power(value_scaled, 4) - 
					2.5882 * power(value_scaled, 3) + 
					4.3953 * power(value_scaled, 2) - 
					3.1896 * value_scaled - 
					0.0092;

		//GREEN
		green =		-0.1748 * power(value_scaled, 6) + 
					1.8527 * power(value_scaled, 5) - 
					7.1468 * power(value_scaled, 4) + 
					12.307 * power(value_scaled, 3) - 
					9.38   * power(value_scaled, 2) + 
					3.0323 * value_scaled - 
					0.0718;

		//BLUE
		blue =		0.1715 * power(value_scaled, 6) - 
					1.6317 * power(value_scaled, 5) + 
					5.7747 * power(value_scaled, 4) - 
					8.8025 * power(value_scaled, 3) + 
					4.4301 * power(value_scaled, 2) + 
					0.7718 * value_scaled + 
					0.0538;					
		break;		
		
	}
	#endregion
	
	#region Color Scale Version 1
	case (1):
	{
		
		//y = 0.0019x6 + 0.0135x5 + 0.0294x4 - 0.1015x3 - 0.2054x2 + 0.7009x + 0.5021
		//y = -0.0027x6 + 0.0064x5 + 0.0584x4 - 0.0542x3 - 0.3363x2 + 0.1161x + 0.828
		//y = 0.0027x6 - 0.0005x5 - 0.0433x4 + 0.0643x3 + 0.2152x2 - 0.3858x + 0.2092

		
		//RED
		red	=		0.0019 * power(value_scaled, 6) + 
					0.0135 * power(value_scaled, 5) + 
					0.0294 * power(value_scaled, 4) - 
					0.1015 * power(value_scaled, 3) - 
					0.2054 * power(value_scaled, 2) + 
					0.7009 * value_scaled + 
					0.5021;
		
		//GREEN
		green	=	-0.0027 * power(value_scaled, 6) + 
					0.0064 * power(value_scaled, 5) + 
					0.0584 * power(value_scaled, 4) - 
					0.0542 * power(value_scaled, 3) - 
					0.3363 * power(value_scaled, 2) + 
					0.1161 * value_scaled + 
					0.828;
		
		//BLUE
		blue	=	0.0027 * power(value_scaled, 6) - 
					0.0005 * power(value_scaled, 5) - 
					0.0433 * power(value_scaled, 4) + 
					0.0643 * power(value_scaled, 3) + 
					0.2152 * power(value_scaled, 2) - 
					0.3858 * value_scaled + 
					0.2092;
		break;
	}
	#endregion
		
	#region Color Scale Version 2
	case (2):
	{

		//RED
		red =		0.0012 * power(value_scaled, 4) - 
					0.1225 * power(value_scaled, 3) + 
					0.913 * power(value_scaled, 2) - 
					1.5318 * value_scaled - 
					0.012;
		
		//GREEN
		green =		0.0322 * power(value_scaled, 5) - 
					0.3557 * power(value_scaled, 4) + 
					1.3901 * power(value_scaled, 3) - 
					2.236 * power(value_scaled, 2) + 
					1.4875 * value_scaled - 
					0.0711;
		
		
		//BLUE
		blue =		-0.0257*power(value_scaled, 5) + 
					0.2716 * power(value_scaled, 4) - 
					0.9548 * power(value_scaled, 3) + 
					1.0006 * power(value_scaled, 2) + 
					0.414 * value_scaled + 
					0.0531;
		break;
	}
	#endregion	
	
	#region Color Scale Version 3
	case (3):
	{
		//RED
		red =		(3.9077 * value_scaled) - 5.7024;
		
		//GREEN
		green =		9.2747 * power(value_scaled, 5) - 
					94.737 * power(value_scaled, 4) + 
					385.23 * power(value_scaled, 3) - 
					778.22 * power(value_scaled, 2) + 
					778.94 * value_scaled - 
					307.35;
		
		//BLUE
		blue =		-0.1953 * power(value_scaled, 5) + 
					1.5149 * power(value_scaled, 4) - 
					3.688 * power(value_scaled, 3) + 
					2.6595 * power(value_scaled, 2) + 
					0.4725 * value_scaled + 
					0.0471;
		
		break;
	}
	#endregion
	
	#region Color Scale Version 4
	case (4):
	{	
		//RED
		red =		(0.7198 * value_scaled) - 0.7103;
		
		//GREEN
		green =		0.0608 * power(value_scaled, 6) - 
					0.3428 * power(value_scaled, 5) + 
					0.4774 * power(value_scaled, 4) + 
					0.2123 * power(value_scaled, 3) - 
					0.8927 * power(value_scaled, 2) + 
					1.0624 * value_scaled + 
					0.0128;
		
		//BLUE
		blue =		0.2155 * power(value_scaled, 5) - 
					1.738 * power(value_scaled, 4) + 
					5.3804 * power(value_scaled, 3) - 
					7.6999 * power(value_scaled, 2) + 
					4.3784 * value_scaled + 
					0.0023;
		break;
	}
	#endregion
	
	#region Color Scale Version 5
	case (5):
	{	
		//RED
		red =		(0.8276 * value_scaled) - 0.4;
		
		//GREEN
		green =		-0.4975 * power(value_scaled, 6) + 
					5.1196 * power(value_scaled, 5) - 
					20.22 * power(value_scaled, 4) + 
					38.816 * power(value_scaled, 3) - 
					38.317 * power(value_scaled, 2) + 
					19.322 * value_scaled  - 
					3.8136;
		
		//BLUE
		blue =		-0.0525 * power(value_scaled, 5) + 
					0.5032 * power(value_scaled, 4) - 
					1.1622 * power(value_scaled, 3) - 
					0.3206 * power(value_scaled, 2) + 
					2.3549 * value_scaled  - 
					0.0236;
		break;
	}
	#endregion
	
	#region Color Scale Version 6
	case (6):
	{
		//RED
		red =		(0.445633 * value_scaled) - 0.2;
		
		//GREEN
		green =		0;
		
		//BLUE
		blue =		(-0.445633 * value_scaled) + 1.2;
		
		break;
	}
	#endregion
	
	#region Color Scale Version 7
	case (7):
	{
		//RED
		red =		(0.6897 * value_scaled) - 1.1667;
		
		//GREEN
		green =		(-0.754 * abs(value_scaled - 1.570796)) + 1.1818;

		//BLUE
		blue =		(-0.5911 * value_scaled) + 1;
		break;
	}
	#endregion
	
	#region Color Scale Version 8
	case (8):
	{
		//RED
		red =		(0.8276 * value_scaled) - 0.2;
		
		//GREEN
		green =		(-0.84 * abs(value_scaled - (pi/2))) + 1.1;
		
		//BLUE
		blue =		(-0.8276 * value_scaled) + 2.4;
		break;
	}
	#endregion
	
	#region Color Scale Version 9
	case (9):
	{	
		//RED
		red =	(0.4531 * value_scaled) - 0.2;
		
		//GREEN
		green =	(0.4531 * value_scaled) - 0.2;
		
		//BLUE
		blue = (0.4531 * value_scaled) - 0.2;
		break;
	}
	#endregion
}

//Convert RGB values from "0-1" scale to "0-255" scale
red = 255 * clamp(red, 0, 1);
green = 255 * clamp(green, 0, 1);
blue = 255 * clamp(blue, 0, 1);

//Pass back a color in GameMaker format
return (make_color_rgb(red, green, blue));
