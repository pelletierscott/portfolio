///scr_unit_converter(temp, output_unit_type)
///scr_unit_converter(temp, output_unit_type)
///@param {real} temp The temperature to convert.
///@param {string} output_unit_type Unit to convert to.


//Declare variables
input_temp = argument0;
unit = argument1;


//Convert Imperial to Metric 
if ((unit == "metric") || (unit == "celcius") || (unit == 0))
{
	return ((input_temp * (9/5)) + 32);
}

//Convert Metric to Imperial
else if ((unit == "imperial") || (unit == "fahrenheit") || (unit == 1))
{
	return ((input_temp - 32) * (5/9));
}

//If output_unit_type is not recognised
else
{
	show_debug_message("Invalid unit type");
	return (0);
}
