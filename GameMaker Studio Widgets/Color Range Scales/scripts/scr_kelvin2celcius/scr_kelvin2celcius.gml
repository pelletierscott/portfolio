///scr_kelvin2celcius(temp_kelvin)
///scr_kelvin2celcius(temp_kelvin)
///@param {real} temp_kelvin A temperature in Kelvin.


//Declare variables
kelvin = argument0;

//Return argument as Celcius
return (kelvin - 273.15);
