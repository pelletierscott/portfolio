///scr_kelvin2fahrenheit(temp_kelvin)
///scr_kelvin2fahrenheit(temp_kelvin)
///@param {real} temp_kelvin A temperature in Kelvin.


//Declare variables
kelvin = argument0;

//Return argument as Fahrenheit
return ((9/5) * (kelvin - 273.15) + 32);
