/// @desc Draw Demonstration instructions


//Draw logo
draw_sprite_ext(spr_ikhan, 0, room_width-100, 50, 1, 1, 0, c_white, 1);

//Set color and alignments
draw_set_color(c_white);
draw_set_halign(fa_left);

//Draw title
draw_text_ext_transformed(20, 30, "RGB COLOR SCALES", 20,500, 1.5, 1.5, 0);

//Draw Instructions and description
draw_text_ext_transformed(	30, 
							100, 
							"This demo showcases a script which takes two values; a number, and a " +
							"scale selection, returning a color code from a linear gradated color " +
							"scale. The demo is setup as a temperature scale but the script is " +
							"applicable to many other uses." +
							"\n\nThe scales are adjustable, the demo provides all the controls necessary " +
							"to preview and adjust the scale." +
							"\n\nThe script contains several different color scales based on the Red-Green-Blue " +
							"(RBG) elements. The default scale resembles the output of an Infrared camera; " +
							"the others are variants on the default.",
							15, 
							700, 
							1.3, 
							1.3, 
							0);

//Draw graphics
draw_sprite_ext(spr_arrows,		0, 173, room_height-380, 1.5, 1.5, 0, c_white, 1)
draw_sprite_ext(spr_numpad,		0, 125, room_height-270, 1.5, 1.5, 0, c_white, 1)
draw_sprite_ext(spr_spacebar,	0, 20, room_height-135, 1.5, 1.5, 0, c_white, 1)
draw_sprite_ext(spr_r_key,		0, 223, room_height-70, 1.5, 1.5, 0, c_white, 1)


//Draw graphics texts
draw_text_ext_transformed(	300, room_height - 380,
							"Use the LEFT/RIGHT arrow keys to change the current temperature value.\n" +
							"Use the UP/DOWN keys to change the current version of the color bar.",
							15, 450, 1.5, 1.5, 0);

draw_text_ext_transformed(	300, room_height - 270,
							"Use the number pad keys [7 ,8 ,9] to increase the minimum, warm, and maximum temperatures.\n" + 
							"Use the number pad keys [1, 2, 3] to decrease the minimum, warm, and maximum temperatures.",
							15, 450, 1.5, 1.5, 0);
							
draw_text_ext_transformed(	300, room_height - 135,
							"Use the space bar to change the temperature units [Kelvin, Celcius, Fahrenheit].",
							15, 450, 1.5, 1.5, 0);							

draw_text_ext_transformed(	300, room_height - 70,
							"Press the 'R' key to reset the room and all temperature settings to default.",
							15, 450, 1.5, 1.5, 0);
							