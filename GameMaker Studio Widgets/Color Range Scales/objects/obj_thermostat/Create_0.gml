/// @desc Define variables for Demo


//Set temperature units to default (Kelvin = 0)
units = 0;

//Set color scale to default (ver. 0)
scale = 0;

/*
		The following four variables are used to make the 
	color scales dynamically adjustable. By creating these 
	variables this object (obj_thermostat) can have a color
	scale with different parameteres than the default.
	
		If these variables don't ALL exist in the object 
	calling the script then the script will provide it's 
	default values instead.
*/
colorrange_current = 100;
colorrange_min = 0;
colorrange_max = 100;
colorrange_mid = 50;
