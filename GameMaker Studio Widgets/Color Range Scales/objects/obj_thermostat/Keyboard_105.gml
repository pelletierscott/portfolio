/// @desc Add to maximum


//Add to maximum
colorrange_max += 1;

//Clamp maximum
colorrange_max = clamp (colorrange_max, colorrange_min, 1000000);