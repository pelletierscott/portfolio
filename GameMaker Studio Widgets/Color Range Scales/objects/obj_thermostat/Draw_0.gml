/// @desc Draw visual color meters and bars


#region Draw demo values as temperatures 
	
	//Create spacer between lines
	vert_push = 20
	
	//Draw labels
	draw_text(20, (1 * vert_push), "Minimum Temperature:");	
	draw_text(20, (2 * vert_push), "Warm Temperature:");
	draw_text(20, (3 * vert_push), "Maximum Temperature:");
	draw_text(25, (4 * vert_push), "Current Temperature:");
	
	//Check which unit to use
	switch (units)
	{
		//Draw data
		case (0):
		{
			draw_text(220, (1 * vert_push),	string(colorrange_min) + " Kelvin");
			draw_text(220, (2 * vert_push),	string(colorrange_mid) + " Kelvin");
			draw_text(220, (3 * vert_push),	string(colorrange_max) + " Kelvin");
			draw_text(225, (4 * vert_push),	string(colorrange_current) + " Kelvin");
			break;
		}
		case (1):
		{
			draw_text(220, (1 * vert_push),	string(scr_kelvin2celcius(colorrange_min)) + " Celcius");
			draw_text(220, (2 * vert_push),	string(scr_kelvin2celcius(colorrange_mid)) + " Celcius");
			draw_text(220, (3 * vert_push),	string(scr_kelvin2celcius(colorrange_max)) + " Celcius");
			draw_text(225, (4 * vert_push),	string(scr_kelvin2celcius(colorrange_current)) + " Celcius");
			break;
		}
		case (2):
		{
			draw_text(220, (1 * vert_push),	string(scr_kelvin2fahrenheit(colorrange_min)) + " Fahrenheit");
			draw_text(220, (2 * vert_push),	string(scr_kelvin2fahrenheit(colorrange_mid)) + " Fahrenheit");
			draw_text(220, (3 * vert_push),	string(scr_kelvin2fahrenheit(colorrange_max)) + " Fahrenheit");
			draw_text(225, (4 * vert_push),	string(scr_kelvin2fahrenheit(colorrange_current)) + " Fahrenheit");
			break;
		}
	}
#endregion

#region Draw color scale label and description

	//Decalre string for color scale description
	var desc_string = ""

	//Fill description string
	switch (scale)
	{
	case (0): { desc_string = "Default \nDefault color scale meant to mimic the infared spectrum."; break;}
	case (1): { desc_string = "Hot Side Only \nThe red side of Version 1 reaching from pure green to incandescent red."; break;}
	case (2): { desc_string = "Cold Side Only \nThe blue side of Version 1 reaching from dark blue to pure green."; break;}
	case (3): { desc_string = "Narrow Middle \nA modification of Version 1 with a narrow middle emphasizing the edge colors."; break;}
	case (4): { desc_string = "Wide Middle \nA modification of Version 1 with a wide middle minimizing the edge colors."; break;}
	case (5): { desc_string = "No Middle \nA modification of Version 1 with no green middle emphasizing the edge colors."; break;}
	case (6): { desc_string = "Pure Red & Blue \nA scale made of Red and Blue color elements."; break;}
	case (7): { desc_string = "Pure RGB elements \nA scale with two color blending (Red, Red & Green, Green, Green & Blue, Blue ."; break;}
	case (8): { desc_string = "White Middle Band\nA modification on version 1 the replaced the middle with pure white."; break;}
	case (9): { desc_string = "Black & White Scale \nA monochromatic scale reaching from Jet black to pure white."; break;}
	}
	
	//Add label to description string
	desc_string = "Color Scale " + string(scale + 1) + ": " +  desc_string ;
	
	//Draw description string
	draw_text_ext(450, 20, desc_string, 20, 500);

#endregion

#region Draw Selected Color Scale bar form

	var bar_length = 800;
	var bar_scale = (colorrange_max - colorrange_min) / bar_length;
	var min_scale = (colorrange_mid - colorrange_min) / (colorrange_max - colorrange_min);
	var max_scale = (colorrange_max - colorrange_mid) / (colorrange_max - colorrange_min);
	

	for (var i = 0; i < bar_length; i = i + 3)
	{
		colorrange_current_trans = colorrange_min + (i * ((colorrange_max - colorrange_min) / bar_length));
		
		draw_line_width_color(	i + 190, 
								130, 
								i + 190,	
								180, 
								3, 
								scr_colorrange(colorrange_current_trans, scale), 
								scr_colorrange(colorrange_current_trans, scale));								
	}
	
#endregion

#region Draw Color Scale forms (Type 1-10)
	
	for (var bars = 0; bars <= 9; bars = bars + 1)
	{
		if (bars == scale)
		{	draw_set_alpha(0.7);	}
		else
		{	draw_set_alpha(1);		}
	
	
		for (var i = 0; i < bar_length; i = i + 3)
		{
			colorrange_current_trans = colorrange_min + (i * ((colorrange_max - colorrange_min) / bar_length));				
		
			draw_line_width_color(	i + 190, 
									200 + (bars * 25), 
									i + 190,	
									220 + (bars * 25), 
									3, 
									scr_colorrange(colorrange_current_trans, bars), 
									scr_colorrange(colorrange_current_trans, bars));
		}
		
		draw_set_alpha(1);	
	}

#endregion

#region Draw color swatch for each bar

	//Draw swatch of currently selected color from currently selected color scale   
	draw_rectangle_color(	40, 130, 90, 180,
							scr_colorrange(colorrange_current, scale),
							scr_colorrange(colorrange_current, scale),
							scr_colorrange(colorrange_current, scale),
							scr_colorrange(colorrange_current, scale),
							false);
	
	//Draw swatches of each color scale
	for (var bars = 0; bars <= 9; bars = bars + 1)
	{
		draw_rectangle_color(	55, 
								200 + (bars * 25),
								75,
								220 + (bars * 25),
								scr_colorrange(colorrange_current, bars),
								scr_colorrange(colorrange_current, bars),
								scr_colorrange(colorrange_current, bars),
								scr_colorrange(colorrange_current, bars),
								false);
	}

#endregion

#region Draw indicators on current color scale
	
	//Declare variable
	var indicator = 0;

	//Check if current value is in the lower range
	if (colorrange_current < colorrange_mid)
	{
		//Calculate the scale of the lower range
		indicator =		((min_scale * bar_length) / 
						(colorrange_mid - colorrange_min)) * 
						(colorrange_current - colorrange_min);
		
	}
	else
	{	
		//Calculate the scale of the upper rnage
		indicator =		(((min_scale*bar_length))) + 
						(((colorrange_current-colorrange_mid) *
						(max_scale*bar_length)) / 
						(colorrange_max-colorrange_mid));
	}
	
	//Draw a line over the current value relative to the scale
	draw_line_color(	indicator + 190,
						130,
						indicator + 190,
						180,
						c_white,
						c_white);	
						
	//Draw a circle over the current value relative to the scale						
	draw_circle_color(	indicator + 190,
						125,
						5,
						c_white,
						c_white,
						false);
						
#endregion

#region Draw R,G,B element graphs

	//Declare Variables for RGB graphs
	var offset = 0;
	var colorrange_current_trans = 0;
	var color = 0;
	
	//Loop through all values between color range's min and max
	for (var i = 0; i < 300; i = i + 3)
	{
		//Calculate the value to pass 
		colorrange_current_trans = colorrange_min + (i * ((colorrange_max - colorrange_min) / 300));	
		
		//Get color
		color = scr_colorrange(colorrange_current_trans, scale);
		
		//Draw graph (RED)	
		offset = 20;		
		draw_line_width_color(		offset + i, 
									700, 
									offset + i, 
									700 - ((color_get_red(color)) * (150/255)), 
									3, c_red, c_red);
	
		//Draw graph (GREEN)
		offset = 340
		draw_line_width_color(		offset + i, 
									700, 
									offset + i, 
									700 - ((color_get_green(color)) * (150/255)), 
									3, c_green, c_green);
		
		//Draw graph (BLUE)
		offset = 660
		draw_line_width_color(		offset + i, 
									700, 
									offset + i, 
									700 - ((color_get_blue(color)) * (150/255)), 
									3, c_blue, c_blue);
	}
	
	//Draw the Axis of all three graphs
	draw_line_color(20,  700, 335, 700, c_white, c_white);
	draw_line_color(20,  700, 20,  500, c_white, c_white);
	draw_line_color(340, 700, 655, 700, c_white, c_white);
	draw_line_color(340, 700, 340, 500, c_white, c_white);
	draw_line_color(660, 700, 975, 700, c_white, c_white);
	draw_line_color(660, 700, 660, 500, c_white, c_white);	

#endregion

#region Draw R,G,B current indicators 

	//Check if value is in the lower range
	if (colorrange_current < colorrange_mid)
	{
		//Calculate the offset in the lower range
		offset = ((min_scale*300) / (colorrange_mid - colorrange_min)) * (colorrange_current - colorrange_min);
	}
	else
	{
		//Calculate the offset in the upper range
		offset = (min_scale*300) + (((colorrange_current - colorrange_mid) * (max_scale * 300)) / (colorrange_max - colorrange_mid));	
	}
	
	//Clamp the offset to fin in the graph
	offset = clamp(offset, 0, 300);
	
	//Draw current color's R, G, B elements as a line on graph
	draw_line_color(20 + offset, 700, 20 + offset, 500, c_white, c_white);
	draw_line_color(340 + offset, 700, 340 + offset, 500, c_white, c_white);
	draw_line_color(660 + offset, 700, 660 + offset, 500, c_white, c_white);	

	//Draw current color's R, G, B elements as value (0 - 255)
	draw_text(20 + offset, 720, color_get_red(scr_colorrange(colorrange_current, scale)));
	draw_text(340 + offset, 720, color_get_green(scr_colorrange(colorrange_current, scale)));
	draw_text(660 + offset, 720, color_get_blue(scr_colorrange(colorrange_current, scale)));
	
#endregion
