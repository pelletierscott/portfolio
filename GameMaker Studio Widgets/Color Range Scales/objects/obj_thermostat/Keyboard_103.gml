/// @desc Add to minimum


//Add to minimum
colorrange_min += 1;

//Clamp minimum
colorrange_min = clamp (colorrange_min, 0, colorrange_max);
