/// @desc Subtract from minimum


//subtract from minimum
colorrange_min -= 1;

//Clamp minimum
colorrange_min = clamp (colorrange_min, 0, colorrange_max);