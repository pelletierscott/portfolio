{
    "id": "99c81e64-7b37-445b-a904-8ea12fd3042b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_thermostat",
    "eventList": [
        {
            "id": "2639a9b3-42d8-406a-9e5a-137bf7d2b164",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "311d1b55-ec09-4982-bf07-6069068e82de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 104,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "49c79822-c14d-4495-9054-22906be1a8e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 98,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "62892b83-a797-460a-ab7a-f0bb839b2c33",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "f52679f5-6bff-4ed2-b512-26f0a56f5a3b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "46625ce4-864b-4c4c-b8f3-c21c5a4dcd1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "9f2172d2-4522-4a26-b148-e922859636f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "02404e52-47af-44d1-aa7f-392510fa709b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 103,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "cc0a74df-4e71-457d-aeaa-85a3de999afc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 97,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "c64c3a2a-d7be-4677-92d3-505b3e125888",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 99,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "149ecffb-2e31-4a68-a1ef-e4eecb825236",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 105,
            "eventtype": 5,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "e56456da-e898-467c-98ee-2c39ba66e31c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "f080183c-db4f-489e-b925-63a9b226a41e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 9,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "bd3cc952-893c-4500-bdb3-d958226a2ddc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 9,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        },
        {
            "id": "02f3c843-4171-440a-b5b8-9fb66c1e81db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 82,
            "eventtype": 9,
            "m_owner": "99c81e64-7b37-445b-a904-8ea12fd3042b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}