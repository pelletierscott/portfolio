/// @desc Switch temperature units


//Cycle through all possible units 
//0 = Kelvin, 1 = Celcius, 2 = Fahrenheit
units =  (units + 1) mod (3);

