/// @desc Adjust all values

/*
	The script itself will clamp any values given to it 
	however for display purposes in this demo the values
	are also clamped in the object.
*/

//Limit the minimum value to values above zero
colorrange_min = max(colorrange_min, 0);

//Limit the maximum value to values within GameMakers integer limit
colorrange_max = min(colorrange_max, 100000000000);

//Clamp the current value between color range's minimum and maximum
colorrange_current = clamp(colorrange_current, colorrange_min, colorrange_max);

//Clamp the color range's middle value between the color range's minimum and maximum
colorrange_mid = clamp(colorrange_mid, colorrange_min, colorrange_max);
