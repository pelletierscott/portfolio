{
    "id": "54a1e112-5422-44c8-888e-6b9427f644e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spacebar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 168,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f8ea89d-ba7e-4811-a096-631a86d60e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54a1e112-5422-44c8-888e-6b9427f644e4",
            "compositeImage": {
                "id": "1a11b803-e875-467f-b7ac-df95518c1b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8ea89d-ba7e-4811-a096-631a86d60e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b3204c8-8a2d-4ea0-bb8d-dbeb18838db7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8ea89d-ba7e-4811-a096-631a86d60e61",
                    "LayerId": "b5ef5591-cb50-45aa-b2ea-d4202ac19a92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "b5ef5591-cb50-45aa-b2ea-d4202ac19a92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54a1e112-5422-44c8-888e-6b9427f644e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 169,
    "xorig": 0,
    "yorig": 0
}