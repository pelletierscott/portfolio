{
    "id": "911e6a96-1e6f-4d48-8f46-9442457d6538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ikhan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 7,
    "bbox_right": 175,
    "bbox_top": 72,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55b18c16-66d7-47ec-b443-e5a74f6d782a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "911e6a96-1e6f-4d48-8f46-9442457d6538",
            "compositeImage": {
                "id": "4f13b982-c0c9-41de-a983-4d165863e678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55b18c16-66d7-47ec-b443-e5a74f6d782a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86eb5110-4ac2-4a35-82d9-6b7792399392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55b18c16-66d7-47ec-b443-e5a74f6d782a",
                    "LayerId": "b74d93c2-62e7-449d-b521-55fbe959d0fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "b74d93c2-62e7-449d-b521-55fbe959d0fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "911e6a96-1e6f-4d48-8f46-9442457d6538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}