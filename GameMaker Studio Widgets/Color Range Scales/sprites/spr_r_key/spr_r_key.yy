{
    "id": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_r_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f8cffb9-bb0c-4c4a-ae48-542848ef2722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
            "compositeImage": {
                "id": "69d96632-1d00-45a0-9181-4f527abee5bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8cffb9-bb0c-4c4a-ae48-542848ef2722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9082c85-2ffa-4e2d-ad51-d971f4ae2611",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8cffb9-bb0c-4c4a-ae48-542848ef2722",
                    "LayerId": "acaa1744-d44a-41f0-8abc-394d2fa04979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 26,
    "layers": [
        {
            "id": "acaa1744-d44a-41f0-8abc-394d2fa04979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32346ea0-58a3-45ce-956c-e0f6a302cbcc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}