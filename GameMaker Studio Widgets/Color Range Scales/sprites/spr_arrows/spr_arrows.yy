{
    "id": "55740a39-7323-4fcc-9ff1-5040504a0296",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrows",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "984fa742-b03a-42b9-b424-cca1c2bf7181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55740a39-7323-4fcc-9ff1-5040504a0296",
            "compositeImage": {
                "id": "2c31f6d4-3d57-462c-bca7-8bc6314d3534",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "984fa742-b03a-42b9-b424-cca1c2bf7181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80ebe8aa-ed5a-4c9d-ac9b-782922152bc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "984fa742-b03a-42b9-b424-cca1c2bf7181",
                    "LayerId": "42f1566c-7ac7-4fbc-9f18-9207b24ea547"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "42f1566c-7ac7-4fbc-9f18-9207b24ea547",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55740a39-7323-4fcc-9ff1-5040504a0296",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}