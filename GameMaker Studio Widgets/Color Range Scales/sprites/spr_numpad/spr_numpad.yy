{
    "id": "1673f5d4-1588-4f3d-8f73-f3c28f6e415c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_numpad",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 0,
    "bbox_right": 96,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "913cefd0-c9ef-44f9-a253-66aa48ee9048",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1673f5d4-1588-4f3d-8f73-f3c28f6e415c",
            "compositeImage": {
                "id": "7095158d-e0cf-4bd5-b254-c8bd17535b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "913cefd0-c9ef-44f9-a253-66aa48ee9048",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5989d52-3fcf-4c9a-9729-787ae7c5a011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "913cefd0-c9ef-44f9-a253-66aa48ee9048",
                    "LayerId": "ef894898-95f3-4562-a6a4-b1322572d0e4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 74,
    "layers": [
        {
            "id": "ef894898-95f3-4562-a6a4-b1322572d0e4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1673f5d4-1588-4f3d-8f73-f3c28f6e415c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 97,
    "xorig": 0,
    "yorig": 0
}