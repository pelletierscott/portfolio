///scr_test_lock(combination)


//Initialize combinations
var combination = string(argument0);
var user_comb = ""; 


//Count All rotors
rotor_total = instance_number(obj_rotor); // - 1;

//Repeat process for each rotor 
for (i = 0; i <= rotor_total; i += 1)
{
    //Cycle through all rotors
    with (obj_rotor)
    {

        
        if (number == other.i)
        {
        
            //show_message("This rotor reads " + string(number));
            //show_message("At digit # " + string(other.i));
            
            user_comb = string_insert(string(position), user_comb, other.i);
            //show_message("User Combination so far " + string(user_comb));
        }
    }
}



if (combination == user_comb)
{
    //show_message("Unlocked");
    //room_restart();
    return true;
}
else
{
    return false;
    //show_message("Incorrect");
}


