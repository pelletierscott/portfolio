﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blackjack
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare variables
            int playersBank = 500;
            int playersBet = 0;
            int dealersSum = 0;
            int playersSum = 0;
            HashSet<Card> playersHand = new HashSet<Card>();
            HashSet<Card> dealersHand = new HashSet<Card>();
            HashSet<int> deckOfCards = new HashSet<int>();

            //Loop while player can still bet
            while (playersBank > 0)
            {
                playersHand.Clear();
                dealersHand.Clear();

                //Loop while trying to place a bet
                while (true)
                {
                    displayBanner();        //Print new screen with banner

                    //Try to convert user bet into integer
                    try
                    {
                        Console.WriteLine("\t   Bank: " + playersBank + "$");
                        Console.Write("\t   Place a bet : ");
                        playersBet = Convert.ToInt32(Console.ReadLine());

                        //Test if bet is in a valid range
                        if (playersBet <= playersBank && playersBet <= 100 && playersBet >= 1)
                        {
                            playersBank -= playersBet;
                            break;
                        }
                        else
                        {
                            Exception ex = new Exception();
                            throw ex;
                        }
                    }
                    catch (Exception ex)
                    {
                        //Invalidate bet
                        Console.WriteLine("\t   Please Enter a valid bet");
                        Console.WriteLine("\t   Press Any key to continue");
                        Console.ReadKey();
                    }
                }

                //Shuffle Deck
                deckOfCards.Clear();

                for (int i = 0; i < 52; i++)
                { deckOfCards.Add(i); }


                //Setup --- First Draw for Player and Dealer
                                               
                //Dealer draws two cards
                for (int i = 0; i < 2; i++)
                {   dealersHand.Add(dealCard(deckOfCards)); }

                //Player draws two cards
                for (int i = 0; i < 2; i++)
                {   playersHand.Add(dealCard(deckOfCards)); }

                //Set default selection for Hit or Stand option
                bool selection = true;

                while (true)    //Players turn
                {


                    bool selected = false;
                    while (!selected)
                    {
                        displayBanner();        //Print new screen with banner
                        displayHands(true, dealersHand, playersHand);
                        displayHitOrStand(selection);

                        //Get user input
                        ConsoleKeyInfo key = Console.ReadKey();

                        //Interprete user input
                        switch (key.Key)
                        {
                            case (ConsoleKey.UpArrow):      selection = !selection;     break;
                            case (ConsoleKey.DownArrow):    selection = !selection;     break;
                            case (ConsoleKey.Spacebar):     selected = true;            break;
                            case (ConsoleKey.Enter):        selected = true;            break;
                        }
                    }


                    if (selection)
                    {
                        //HIT
                        //Draw Card
                        playersHand.Add(dealCard(deckOfCards));

                        //Calculate Sum
                        playersSum = tabulateCardValues(playersHand);

                        if (playersSum >= 21)
                        {   break;    }


                    }
                    else
                    {
                        //Stand
                        playersSum = tabulateCardValues(playersHand);
                        break;
                    }
                }


                if (playersSum < 21)
                {
                    while (true)    //Dealers Turn
                    {
                        displayBanner();        //Print new screen with banner
                        displayHands(false, dealersHand, playersHand);

                        System.Threading.Thread.Sleep(2000);

                        //Calculate Sum
                        dealersSum = tabulateCardValues(dealersHand);

                        //Test Sum
                        if (dealersSum >= 21)
                        { break; }

                        if (dealersSum >= 17)
                        { break; }
                        else
                        { dealersHand.Add(dealCard(deckOfCards)); }


                    }
                }

















                //Compare sums
                displayBanner();        //Print new screen with banner
                displayHands(false, dealersHand, playersHand);
                string outcome = "";

                if (dealersSum > 21)    //Dealer Busts
                {
                    if (playersSum <= 21)
                    {
                        outcome = "\t   " + "You Won --- " + (2 * playersBet) + "$";
                        playersBank += (2 * playersBet);
                    }
                    else
                    {
                        outcome = "\t   " + "Tie";
                        playersBank += playersBet;
                    }
                }

                if (dealersSum == 21)   //Dealer BlackJack
                {
                    if (playersSum == 21)
                    {
                        outcome = "\t   " + "Tie";
                        playersBank += playersBet;
                    }
                    else
                    {
                        outcome = "\t   " + "Dealer Won";
                    }
                }

                if (dealersSum < 21)    //Full Check
                {
                    //player beats dealer
                    if (playersSum > dealersSum)
                    {
                        //player busts
                        if (playersSum > 21)
                        {
                            outcome = "\t   " + "Dealer Won";
                        }
                        else
                        {
                            outcome = "\t   " + "You Won --- " + (2 * playersBet) + "$";
                            playersBank += (2 * playersBet);
                        }
                    }
                    else
                    {

                    }
                    //dealer beats player
                    if (playersSum <= dealersSum)
                    {
                        outcome = "\t   " + "Dealer Won";
                    }
                }

                Console.WriteLine("\t   " + outcome);
                System.Threading.Thread.Sleep(3000);

                Console.ReadKey();
            }
        }

        private static void displayHands(bool faceDown, HashSet<Card> dealersHand, HashSet<Card> playersHand)
        {
            Console.WriteLine("\t   Dealer");

            if (faceDown)
            {
                Console.WriteLine("\t " + dealersHand.ElementAt(0).name);
                Console.WriteLine("\t " + "Facedown Card");
            }
            else
            {
                foreach (Card card in dealersHand)
                {
                    Console.WriteLine("\t " + card.name);
                }
            }

            Console.WriteLine("\n\t   Player");

            foreach (Card card in playersHand)
            {
                Console.WriteLine("\t " + card.name);
            }
        }





        private static void displayHitOrStand(bool selection)
        {
            string hit = "HIT";
            string stand = "STAND";

            if (selection)
            {
                hit = "> " + hit;
                stand = "  " + stand;
            }
            else
            {
                hit = "  " + hit;
                stand = "> " + stand;
            }

            Console.WriteLine("\n\n\t   " + hit + "\n\t   " + stand);
        }






        //Refresh Console and print Main Banner
        private static void displayBanner()
        {
            Console.Clear();
            Console.WriteLine("=============================================");
            Console.WriteLine("\n\n\t  BlackJack");
            Console.WriteLine("\n\n\t  By: Scott Pelletier");
            Console.WriteLine("\n\t  February 6th 2019");
            Console.WriteLine("=============================================");
            Console.WriteLine("\n\n");
        }



        private static int tabulateCardValues(HashSet<Card> hand)
        {
            int sum = 0;
            int aces = 0;

            foreach (Card card in hand)
            {
                if (card.value == 1)
                {
                    aces += 1;
                }
                else
                {   sum += card.value;  }
            }

            for (int i = 0; i < aces; i++)
            {
                if ((sum + 11) > 21)
                { sum += 1; }
                else
                { sum += 11; }
            }

            return sum;
        }






        private static Card dealCard(HashSet<int> deckOfCards)
        {
            Random random = new Random();

            int randomCard = deckOfCards.ElementAt(random.Next(deckOfCards.Count));

            Card newCard = new Card(randomCard);

            deckOfCards.Remove(randomCard);

            return newCard;
        }
    }
}
