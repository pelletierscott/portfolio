﻿using System;

namespace blackjack
{
    internal class Card
    {
        public int number { get; set; }
        public string name { get; set; }
        public int value { get; set; }


        public Card(int number)
        {
            this.number = number;
            this.name = getName(number);
            this.value = getValue(number);
        }

        private int getValue(int number)
        {
            int cardValue = 0;

            //Get cards value as blackjack pip
            switch (((number % 52) % 13) + 1)
            {
                case (1): cardValue = 1;                            break;
                case (11): cardValue = 10;                          break;
                case (12): cardValue = 10;                          break;
                case (13): cardValue = 10;                          break;
                default: cardValue = (((number % 52) % 13) + 1);    break;
            }

            //Return card value
            return cardValue;
        }

        private string getName(int number)
        {
            //Get cards deck number
            string deck = ((number / 52) + 1).ToString();

            //Get cards suit
            string suit = "";
            switch ((number % 52) / 13)
            {
                case (0): suit = "Hearts"; break;
                case (1): suit = "Clubs"; break;
                case (2): suit = "Diamonds"; break;
                case (3): suit = "Spades"; break;
            }

            //Get cards face value
            string face = "";
            switch (((number % 52) % 13) + 1)
            {
                case (1): face = "Ace"; break;
                case (11): face = "Jack"; break;
                case (12): face = "Queen"; break;
                case (13): face = "King"; break;
                default: face = (((number % 52) % 13) + 1).ToString(); break;
            }

            //Return card name
            return (face + " of " + suit);
        }



    }



}