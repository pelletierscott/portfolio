﻿using LiteDB;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;
using System.Windows.Media.Animation;

namespace NotesInterface
{
    /// <summary>
    /// Run Main Window
    /// </summary>
    public partial class MainWindow : Window
    {
        //Declare variables
        string directory;
        List<string> supertypes = new List<string>();
        List<string> types = new List<string>();
        List<string> subtypes = new List<string>();
        int slide_id;
        int slide_min = 0;
        int slide_current = 0;
        int slide_max;
        bool dracula = true;
        string supertype = null;
        string type = null;
        string subtype = null;
        string slidetitle = null;
        string filetype1 = null;
        string filetype2 = null;
        byte[] rawfile1 = null;
        byte[] rawfile2 = null;
        GIFObj rightGif = null;
        GIFObj leftGif = null;
        static System.Timers.Timer timer;
        double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
        double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
        
        /// <summary>
        /// Run Main Window (inner)
        /// </summary>
        public MainWindow()
        {
            //Initialize Form
            InitializeComponent();

            //Initialize Timer
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.AutoReset = true;
            timer.Elapsed += GIFPainter;

            //Resize Window
            this.Height = screenHeight;
            this.Width = screenWidth / 2;

            //Resize Slide Grid
            slide_grid.Width = this.Width - 70;
            slide_grid.Height = this.Height * 0.6;

            //Position Window
            this.Left = screenWidth - this.Width;
            this.Top = 0;

            //Load all supertypes (languages) from DB
            Load();
            Tab_supertypes.SelectedIndex = 0;
            Tab_types.SelectedIndex = 0;
            Tab_subtypes.SelectedIndex = 0;                
        }

        /// <summary>
        /// Initialize Program's supertypes
        /// </summary>
        /// <returns>DB file path</returns>
        private void Load()
        {
            //Push Custom Dialog
            ConnectionChooser connectionChooser = new ConnectionChooser();
            connectionChooser.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Test if Connection Chooser dialog worked
            if (connectionChooser.ShowDialog() == true)
            {
                //Assign DB file path
                directory = connectionChooser.Conn;
            }
            else
            {
                //Close program
                Environment.Exit(1);
            }

            //Try to Connection to DB
            try
            {
                // Open database (or create if doesn't exist)
                using (var db = new LiteDatabase(directory))
                {
                    // Clear Languages
                    supertypes.Clear();                              

                    // Get a collection (or create, if doesn't exist)
                    var col = db.GetCollection<Slide>("note");

                    //Get all distinct supertypes
                    var results = col.FindAll();

                    //Populate supertype list
                    for (int i = 0; i < results.Count(); i++)
                    {
                        //Test if supertype is unique
                        if (!supertypes.Contains((results.Skip(i).First().supertype)))
                        {
                            //Add supertype from DB to list of supertypes
                            supertypes.Add(results.Skip(i).First().supertype);
                        }
                    }
                    //Alphabetize supertype list
                    supertypes.Sort();

                    //Push supertype list to supertype tab
                    Tab_supertypes.ItemsSource = supertypes;
                    Tab_supertypes.Items.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("The DB you selected is empty or couldn't be read");
            }
        }

        /// <summary>
        /// Load types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tab_supertypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Test if function call came from supertype control
            if (e.OriginalSource == Tab_supertypes)
            {
                //Test if source of function call is supertype control
                if (Tab_supertypes.SelectedIndex != -1)
                {
                    //Clear next level of menu (type)
                    types.Clear();

                    //Get current supertype
                    supertype = Tab_supertypes.Items.GetItemAt(Tab_supertypes.SelectedIndex).ToString();

                    //Open database (or create if doesn't exist)
                    using (var db = new LiteDatabase(directory))
                    {
                        //Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all types matching supertype
                        var results = col.Find(x => x.supertype.Equals(supertype));

                        //Populate type list
                        for (int i = 0; i < results.Count(); i++)
                        {
                            //Test for match with type list and supertype selection
                            if (!types.Contains((results.Skip(i).First().type)) && results.Skip(i).First().supertype == supertype)
                            {
                                //Add match to type list
                                types.Add(results.Skip(i).First().type);
                            }
                        }
                        //Alphabetize type list
                        types.Sort();
                    }

                    //Push list to tab
                    Tab_types.ItemsSource = types;
                    Tab_types.Items.Refresh();

                    //Test if selection is default (-1)
                    if (Tab_types.SelectedIndex == -1)
                    {
                        //Switch to first index (0)
                        Tab_types.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Load subtypes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tab_types_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Test if source of function call is type control
            if (e.OriginalSource == Tab_types || e.OriginalSource == Tab_supertypes)
            {
                //Test if a valid tab is seleted
                if (Tab_types.SelectedIndex != -1)
                {
                    //Clear subtype tab
                    subtypes.Clear();

                    //Get current supertype selection
                    supertype = Tab_supertypes.Items.GetItemAt(Tab_supertypes.SelectedIndex).ToString();

                    //Get current type selection
                    type = Tab_types.Items.GetItemAt(Tab_types.SelectedIndex).ToString();

                    //Open database (or create if doesn't exist)
                    using (var db = new LiteDatabase(directory))
                    {
                        //Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all subtypes matching supertype and type
                        var results = col.Find(x => x.supertype.Equals(supertype) && x.type.Equals(type));

                        //Populate subtype list
                        for (int i = 0; i < results.Count(); i++)
                        {
                            //Test for match with subtype list, supertype selection, and type selection
                            if (!subtypes.Contains((results.Skip(i).First().subtype))
                                && results.Skip(i).First().supertype == supertype
                                && results.Skip(i).First().type == type)
                            {
                                //Add match to type list
                                subtypes.Add(results.Skip(i).First().subtype);
                            }
                        }
                        //Alphabetize subtype list
                        subtypes.Sort();
                    }

                    //Push list to tab
                    Tab_subtypes.ItemsSource = subtypes;
                    Tab_subtypes.Items.Refresh();

                    //Test if selection is default (-1)
                    if (Tab_subtypes.SelectedIndex == -1)
                    {
                        //Switch to first index (0)
                        Tab_subtypes.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Load slides information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tab_subtypes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Test if function call comes from subtype control
            if (e.OriginalSource == Tab_subtypes || e.OriginalSource == Tab_types)
            {
                //Test if a valid tab is seleted
                if (Tab_subtypes.SelectedIndex != -1)
                {
                    //Get supertype, type, subtype
                    supertype = Tab_supertypes.Items.GetItemAt(Tab_supertypes.SelectedIndex).ToString();
                    type = Tab_types.Items.GetItemAt(Tab_types.SelectedIndex).ToString();
                    subtype = Tab_subtypes.Items.GetItemAt(Tab_subtypes.SelectedIndex).ToString();

                    //Open database (or create if doesn't exist)
                    using (var db = new LiteDatabase(directory))
                    {
                        //Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all slides matching supertype, type, and subtype
                        var results = col.Find(x => x.supertype.Equals(supertype) && x.type.Equals(type) && x.subtype.Equals(subtype));
                                               
                        //Set current slide to default (0)
                        slide_current = 0;

                        //Count number of slides under subtype
                        slide_max = col.Count(Query.And(
                            Query.EQ("supertype", supertype),
                            Query.EQ("type", type),
                            Query.EQ("subtype", subtype)));

                        //Change slide max to 0-origin number line
                        slide_max -= 1;

                        //Setup slide buttons
                        slide_button_lock();

                        //Load slide
                        push_slide();
                    }
                }
            }
        }

        /// <summary>
        /// Manage slide buttons
        /// </summary>
        private void slide_button_lock()
        {
            //Test if there are more slides 
            if (slide_current == slide_max)
            {
                //Hide next slide button
                next_slide.IsEnabled = false;
                next_slide.Visibility = Visibility.Hidden;
            }
            else
            {
                //Unhide next slide button
                next_slide.IsEnabled = true;
                next_slide.Visibility = Visibility.Visible;
            }

            //Test if this is first slide
            if (slide_current == slide_min)
            {
                //Hide previous slide button
                previous_slide.IsEnabled = false;
                previous_slide.Visibility = Visibility.Hidden;
            }
            else
            {
                //Unhide previous slide button
                previous_slide.IsEnabled = true;
                previous_slide.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Load current slide
        /// </summary>
        private void push_slide()
        {
            //Get supertype, type, and subtype 
            supertype = Tab_supertypes.Items.GetItemAt(Tab_supertypes.SelectedIndex).ToString();
            type = Tab_types.Items.GetItemAt(Tab_types.SelectedIndex).ToString();
            subtype = Tab_subtypes.Items.GetItemAt(Tab_subtypes.SelectedIndex).ToString();

            //Unhide updae & delete buttons
            update.IsEnabled = true;
            delete.IsEnabled = true;

            //Open database (or create if doesn't exist)
            using (var db = new LiteDatabase(directory))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Slide>("note");

                //Get matching slide
                var result = col.Find(Query.And(
                    Query.EQ("supertype", supertype),
                    Query.EQ("type", type),
                    Query.EQ("subtype", subtype),
                    Query.EQ("slide_num", slide_current)));

                //Get slide data    
                slidetitle = result.First().slideName;
                slide_title.Text = slidetitle;
                slide_id = result.First().Id;
                filetype1 = result.First().fileName1;
                filetype2 = result.First().fileName2;
                rawfile1 = result.First().file1;

                //Try to load second file
                try
                {
                    //Attempt to get file 2 data
                    rawfile2 = result.First().file2;
                }
                catch (InvalidCastException ex)
                {
                    //Output exception to console
                    Console.Write(ex);
                }
            }

            //Setup slide panels
            slide_grid.Children.Clear();
            export_L.IsEnabled = false;
            export_R.IsEnabled = false;

            //Test if file2 need to be loaded
            if (filetype2.Equals("void"))
            {
                //Setup single panel slide
                push_slide_inner(filetype1, rawfile1, 2, 0);
                export_L.IsEnabled = true;
            }
            else
            {
                //Setup double panel slide
                push_slide_inner(filetype1, rawfile1, 1, 0);
                push_slide_inner(filetype2, rawfile2, 1, 1);
                export_L.IsEnabled = true;
                export_R.IsEnabled = true;
            }
        }

        /// <summary>
        /// Transfer slide information to slide panel
        /// </summary>
        /// <param name="filetype"></param>
        /// <param name="rawfile"></param>
        /// <param name="span"></param>
        /// <param name="column"></param>
        private void push_slide_inner(string filetype, byte[] rawfile, int span, int column)
        {
            //Parse by filetype
            switch (filetype)
            {
                //Read as plain text file
                case (".txt"):
                    {
                        //Configure panel
                        TextBox textbox = new TextBox();
                        textbox.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                        textbox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                        textbox.FontSize = 15;
                        textbox.MaxHeight = slide_grid.Height;
                        textbox.IsReadOnly = true;

                        //Load panel content
                        textbox.Text = Encoding.UTF8.GetString(rawfile);

                        //Add panel
                        slide_grid.Children.Add(textbox);
                        Grid.SetColumnSpan(textbox, span);
                        Grid.SetColumn(textbox, column);
                        Grid.SetRow(textbox, 0);
                        break;
                    }

                //Read as portable network graphic
                case (".png"):
                    {
                        //Configure panel
                        Image imagePNG = new Image();
                        imagePNG.Stretch = Stretch.Fill;

                        //Test if panel is empty or invalid
                        if (rawfile == null || rawfile.Length == 0)
                        {
                            //Cancel File load and cancel display
                            return;
                        }

                        //Load panel content
                        imagePNG.Source = LoadImage(rawfile);

                        //Add panel
                        slide_grid.Children.Add(imagePNG);
                        Grid.SetColumnSpan(imagePNG, span);
                        Grid.SetColumn(imagePNG, column);
                        Grid.SetRow(imagePNG, 0);
                        break;
                    }

                //Read as bitmap image
                case (".bmp"):
                    {
                        //Configure panel
                        Image imageBMP = new Image();
                        imageBMP.Stretch = Stretch.Fill;

                        //Test if panel is empty or invalid
                        if (rawfile == null || rawfile.Length == 0)
                        {
                            //Cancel File load and cancel display
                            return;
                        }

                        //Load panel content
                        imageBMP.Source = LoadImage(rawfile);

                        //Add panel
                        slide_grid.Children.Add(imageBMP);
                        Grid.SetColumnSpan(imageBMP, span);
                        Grid.SetColumn(imageBMP, column);
                        Grid.SetRow(imageBMP, 0);
                        break;
                    }

                case (".gif"):
                    {
                        //Test if panel is empty or invalid
                        if (rawfile == null || rawfile.Length == 0)
                        {
                            //Cancel File load and cancel display
                            return;
                        }

                        //Buffer Gif from binary to stream
                        Stream imageStreamSource = new MemoryStream(rawfile);

                        //Create decoder to read Gif stream
                        GifBitmapDecoder decoder = new GifBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                        //Create container for Gif data
                        GIFObj gifObj = new GIFObj();

                        gifObj.GIFImages = new List<Image>();                   //Create collection for Gif images

                        //Loop through and decode images in Gif
                        for (int i = 0; i < decoder.Frames.Count; i++)
                        {
                            BitmapSource bitmapSource = decoder.Frames[i];      //Decode Gif frame as bitmap
                            Image image = new Image();                          //Create new image
                            image.Source = bitmapSource;                        //Transfer bitamp to image
                            gifObj.GIFImages.Add(image);                        //Add image to collection
                        }

                        //Pull first image from collection and assign to UI Element
                        gifObj.GIFElement = gifObj.GIFImages.First();
                        gifObj.GIFFrame = 0;
                        Image imageGIF = gifObj.GIFImages.Skip(gifObj.GIFFrame).First();

                        //Configure timer
                        if (timer == null)
                        {
                            timer = new System.Timers.Timer();
                            timer.Interval = 1000;
                            timer.AutoReset = true;
                            timer.Elapsed += GIFPainter;
                        }

                        timer.Start();                                          //Start timer
                        slide_grid.Children.Add(imageGIF);                      //Add Element to grid
                        imageGIF.Stretch = Stretch.Fill;                        //Stretch Image across panel
                        Grid.SetColumnSpan(imageGIF, span);                     //Set Elements span 
                        Grid.SetColumn(imageGIF, column);                       //Set Elements column
                        Grid.SetRow(imageGIF, 0);                               //Set Elements row


                        if (column == 0) {  leftGif = gifObj;   }


                        if (column == 1) {  rightGif = gifObj;  }

                        //leftGif = (column == 0) ? gifObj : leftGif;             //Check and assign .gif (left) 
                        //rightGif = (column == 1) ? gifObj : rightGif;           //Check and assign .gif (right)

                        break;
                    }

                //Read as Joint Photo Group image (JPG)
                case (".jpg"):
                    {
                        //Configure panel
                        Image imageJPG = new Image();
                        imageJPG.Stretch = Stretch.Fill;

                        //Test if panel is empty or invalid
                        if (rawfile == null || rawfile.Length == 0)
                        {
                            //Cancel File load and cancel display
                            return;
                        }

                        //Load panel content
                        imageJPG.Source = LoadImage(rawfile);

                        //Add panel
                        slide_grid.Children.Add(imageJPG);
                        Grid.SetColumnSpan(imageJPG, span);
                        Grid.SetColumn(imageJPG, column);
                        Grid.SetRow(imageJPG, 0);
                        break;
                    }

                //Read as invalid
                default:
                    {
                        //Configure panel
                        TextBox invalidFileType = new TextBox();
                        invalidFileType.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                        invalidFileType.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                        invalidFileType.FontSize = 25;
                        invalidFileType.Foreground = Brushes.Red;
                        invalidFileType.MaxHeight = slide_grid.Height;
                        invalidFileType.IsReadOnly = true;

                        //Load panel content
                        invalidFileType.Text = Encoding.UTF8.GetString(rawfile);

                        //Add panel
                        slide_grid.Children.Add(invalidFileType);
                        Grid.SetColumnSpan(invalidFileType, span);
                        Grid.SetColumn(invalidFileType, column);
                        Grid.SetRow(invalidFileType, 0);
                        break;
                    }
            }

            //Apply visual schema
            foreach (var child in slide_grid.Children)
            {
                //Test if panel is text-based
                if (child is TextBox)
                {
                    //Test visual schema
                    if (dracula)
                    {
                        //Bright
                        TextBox textBox = (TextBox)child;
                        textBox.Background = Brushes.White;
                        textBox.Foreground = Brushes.Black;
                    }
                    else
                    {
                        //Dark
                        TextBox textBox = (TextBox)child;
                        textBox.Background = Brushes.Black;
                        textBox.Foreground = Brushes.White;
                    }
                }
            }
        }

        /// <summary>
        /// Manage GIF Animations
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void GIFPainter(Object source, ElapsedEventArgs e)
        {
            //Run function in parallel with Main
            this.Dispatcher.Invoke(() =>
            {
                //Test if any frames are .gifs
                if (filetype1 == ".gif" || filetype2 == ".gif")
                {
                    //Test if left panel .gif need updating
                    if (leftGif != null)
                    {
                        //Remove current frame
                        slide_grid.Children.Remove(leftGif.GIFElement);

                        //Configure frame
                        leftGif.GIFFrame = ((leftGif.GIFFrame + 1) >= leftGif.GIFImages.Count) ? 0 : leftGif.GIFFrame + 1;
                        Image imageGIF = leftGif.GIFImages.Skip(leftGif.GIFFrame).First();
                        leftGif.GIFElement = leftGif.GIFImages.Skip(leftGif.GIFFrame).First();
                        imageGIF.Stretch = Stretch.Fill;
                        Grid.SetRow(imageGIF, 0);
                        Grid.SetColumn(imageGIF, 1);
                        Grid.SetColumnSpan(imageGIF, 2);
                        Grid.SetColumn(imageGIF, leftGif.column);
                        Grid.SetRow(imageGIF, 0);

                        //Add frame
                        slide_grid.Children.Add(imageGIF);
                    }

                    //Test if right panel .gif need updating
                    if (rightGif != null)
                    {
                        //Remove current frame
                        slide_grid.Children.Remove(rightGif.GIFElement);

                        //Configure frame
                        rightGif.GIFFrame = ((rightGif.GIFFrame + 1) >= rightGif.GIFImages.Count) ? 0 : rightGif.GIFFrame + 1;                        
                        Image imageGIF = rightGif.GIFImages.Skip(rightGif.GIFFrame).First();
                        rightGif.GIFElement = rightGif.GIFImages.Skip(rightGif.GIFFrame).First();
                        imageGIF.Stretch = Stretch.Fill;
                        Grid.SetRow(imageGIF, 0);
                        Grid.SetColumn(imageGIF, 1);
                        Grid.SetColumnSpan(imageGIF, 1);
                        Grid.SetColumn(imageGIF, rightGif.column + 1);
                        Grid.SetRow(imageGIF, 0);

                        //Add frame
                        slide_grid.Children.Add(imageGIF);
                    }

                    
                }
                //Test if .gif timer is enabled 
                else if (timer.Enabled == true)
                {
                    //Stop timer and clear .gif containers
                    timer.Stop();
                    leftGif = null;
                    rightGif = null;
                }
            });
        }

        /// <summary>
        /// Load image based panel contents
        /// </summary>
        /// <param name="rawfile"></param>
        /// <returns></returns>
        private ImageSource LoadImage(byte[] rawfile)
        {
            //Create image object
            BitmapImage bitmap = new BitmapImage();

            //Access rawfile as stream
            using (var mem = new MemoryStream(rawfile))
            {
                //Load fileimage into image container (mem ---> bitmap)
                mem.Position = 0;
                bitmap.BeginInit();
                bitmap.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                bitmap.CacheOption = BitmapCacheOption.OnLoad;
                bitmap.UriSource = null;
                bitmap.StreamSource = mem;
                bitmap.EndInit();
            }
            //Define the image as unmodifiable (freeze)
            bitmap.Freeze();
            return bitmap;
        }

        /// <summary>
        /// Focus on previous slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void previous_slide_Click(object sender, RoutedEventArgs e)
        {
            //Interate to previous slide number
            slide_current -= 1;

            //Refresh slide data
            push_slide();

            //Refresh slide navigation buttons
            slide_button_lock();
        }
           
        /// <summary>
        /// Focus on next slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void next_slide_Click(object sender, RoutedEventArgs e)
        {
            //Interate to next slide number
            slide_current += 1;

            //Refresh slide data
            push_slide();

            //Refresh slide navigation buttons
            slide_button_lock();
        }

        /// <summary>
        /// Add new slide object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_add(object sender, RoutedEventArgs e)
        {
            //Create new Slide Form
            SlideEditor slideEditor = new SlideEditor(directory);

            //Position Slide Form
            slideEditor.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Run Slide Form
            if (slideEditor.ShowDialog() == true)
            {
                //Adjust maximum slide count by +1
                slide_max += 1;

                //Refresh the currently selected Slide
                Refresh(slideEditor.newSlide);
            }
        }

        /// <summary>
        /// Update currently focused slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_update(object sender, RoutedEventArgs e)
        {
            //Create new Slide Form with Slide to update
            SlideEditor slideEditor = new SlideEditor(directory, slide_id);

            //Position Slide Form
            slideEditor.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Run Slide Form
            if (slideEditor.ShowDialog() == true)
            {
                //Adjust maximum slide count by -1
                //slide_max -= 1;

                Tab_supertypes.SelectedIndex = -1;

                //Refresh the currently selected Slide
                Refresh(slideEditor.slideToUpdate);
            }
        }  

        /// <summary>
        /// Delete currently focused slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_delete(object sender, RoutedEventArgs e)
        {
            //Double check Delete operation with
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete this slide?\n\n" + slidetitle + "\n", "Delete Slide", MessageBoxButton.YesNo);

            switch (result)
            {
                //Operation confirmed - Delete slide
                case (MessageBoxResult.Yes):

                    //Try to Connection to DB
                    try
                    {
                        //Open database (or create if doesn't exist)
                        using (var db = new LiteDatabase(directory))
                        {
                            //Get a collection (or create, if doesn't exist)
                            var col = db.GetCollection<Slide>("note");

                            //Delete Target Slide
                            col.Delete(x => x.Id == slide_id);
                            
                            //Get all slide is same subtype
                            var results = col.Find(x => 
                                x.supertype.Equals(supertype) && 
                                x.type.Equals(type) && 
                                x.subtype.Equals(subtype));

                            //Loop through each matching slide
                            foreach (Slide slide in results)
                            {
                                //Test if slide is effected
                                if (slide.slide_num > slide_current)
                                {
                                    //Modify slide order
                                    slide.slide_num -= 1;
                                    col.Update(slide);
                                }
                            }

                            //Adjust maximum slide count by -1
                            slide_max -= 1;

                            //Re-arrange Db for optimal file size
                            db.Shrink();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Tell user deletion failed
                        MessageBox.Show("Deletion Failed\n" +
                            "- Connection to the Database failed\n" +
                            "- Targeted slide doesn't exist\n");
                    }

                    //Refresh slide to circumvent deleted Slide
                    Refresh();
                    break;

                //Delete operation cancelled - return
                case MessageBoxResult.No:
                    return; 
            }
        }

        /// <summary>
        /// Export contents of Left/Central Panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_export_L(object sender, RoutedEventArgs e)
        {
            //Create and Open dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = supertype + " " + type + " " + subtype + " " + slidetitle + "-Left";
            saveFileDialog.DefaultExt = filetype1;
            saveFileDialog.Filter = "(" + filetype1 + ")|*" + filetype1;
            saveFileDialog.ValidateNames = false;

            //Test if dialog succeded
            if (saveFileDialog.ShowDialog() == true)
            {
                //Initialize variables
                string destinationPath = saveFileDialog.FileName;
                System.IO.FileStream saveFileStream;

                //Write Slide panel to File
                try
                {
                    //Set file stream parameters
                    saveFileStream = new System.IO.FileStream(destinationPath,
                                                                    System.IO.FileMode.Create,
                                                                    System.IO.FileAccess.Write,
                                                                    System.IO.FileShare.ReadWrite);

                    //Write file(left) and close
                    saveFileStream.Write(rawfile1, 0, rawfile1.Length);
                    saveFileStream.Close();
                }
                catch (Exception ex)
                {
                    //Manage export failure
                    MessageBox.Show("Invalid file path: Export failed\n" +
                        "- You don't have access to the directory\n" +
                        "- The file path was invalid\n" +
                        "- The data could not be read\n");
                } 
            }
        }

        /// <summary>
        /// Export contents of Right Panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_export_R(object sender, RoutedEventArgs e)
        {
            //Create and Open dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = supertype + " " + type + " " + subtype + " " + slidetitle + "-Right";
            saveFileDialog.DefaultExt = filetype1;
            saveFileDialog.Filter = "(" + filetype2 + ")|*" + filetype2;
            saveFileDialog.ValidateNames = false;

            //Test if dialog succeded
            if (saveFileDialog.ShowDialog() == true)
            {
                //Initialize variables
                string destinationPath = saveFileDialog.FileName;
                System.IO.FileStream saveFileStream;

                try
                {
                    //Set file stream parameters
                    saveFileStream = new System.IO.FileStream(destinationPath,
                                                                    System.IO.FileMode.Create,
                                                                    System.IO.FileAccess.Write,
                                                                    System.IO.FileShare.ReadWrite);

                    //Write file(right) and close
                    saveFileStream.Write(rawfile2, 0, rawfile2.Length);
                    saveFileStream.Close();

                }
                catch (Exception ex)
                {
                    //Manage export failure
                    MessageBox.Show("Invalid file path: Export failed\n" +
                        "- You don't have access to the directory\n" +
                        "- The file path was invalid\n" +
                        "- The data could not be read\n");
                }
            }
        }

        /// <summary>
        /// Update visual schema (set to Dark)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dracula_Unchecked(object sender, RoutedEventArgs e)
        {
            //Loop through each panel
            foreach (var child in slide_grid.Children)
            {
                //Test if panel is text-based
                if (child is TextBox)
                {
                    //Light
                    TextBox textBox = (TextBox)child;
                    textBox.Background = Brushes.White;
                    textBox.Foreground = Brushes.Black;
                    dracula = true;
                }
            }
        }

        /// <summary>
        /// Update visual schema (set to Dark)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dracula_Checked(object sender, RoutedEventArgs e)
        {
            //Loop through each panel
            foreach (var child in slide_grid.Children)
            { 
                //Test if panel is text-based
                if (child is TextBox)
                {
                    //Dark
                    TextBox textBox = (TextBox)child;
                    textBox.Background = Brushes.Black;
                    textBox.Foreground = Brushes.White;
                    dracula = false;
                }
            }
        }

        /// <summary>
        /// Generic refresh operation (for Post Delete)
        /// </summary>
        private void Refresh()
        {
            //Access Database
            using (var db = new LiteDatabase(directory))
            {
                //Get database as collection
                var col = db.GetCollection<Slide>("note");

                //Test if another slide in subtype is availible
                if (col.Count(x => x.subtype == subtype) == 0)
                {
                    //Test if a subtype is availible
                    if (col.Count(x => x.type == type) == 0)
                    {
                        //Test if a type is availible
                        if (col.Count(x => x.supertype == supertype) == 0)
                        {
                            //Test if a supertype is availible
                            if (col.Count() == 0)
                            {
                                //Present warning and blank interface
                                MessageBoxResult result = MessageBox.Show("No other slides where found");

                                //Purge all tab controls
                                supertypes.Clear();
                                types.Clear();
                                subtypes.Clear();
                                Tab_supertypes.ItemsSource = supertypes;
                                Tab_types.ItemsSource = types;
                                Tab_subtypes.ItemsSource = subtypes;
                                Tab_supertypes.Items.Refresh();
                                Tab_types.Items.Refresh();
                                Tab_subtypes.Items.Refresh();

                                //Clear all Panels
                                slide_grid.Children.Clear();

                                //Clear Slides title
                                slide_title.Text = "";

                                //Hide Slide buttons
                                next_slide.IsEnabled = false;
                                next_slide.Visibility = Visibility.Hidden;
                                previous_slide.IsEnabled = false;
                                previous_slide.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                //Reload Supertypes
                                // Clear Languages
                                supertypes.Clear();

                                //Get all distinct supertypes
                                var results = col.FindAll();

                                //Populate supertype list
                                for (int i = 0; i < results.Count(); i++)
                                {
                                    //Test if supertype is unique
                                    if (!supertypes.Contains((results.Skip(i).First().supertype)))
                                    {
                                        //Add supertype from DB to list of supertypes
                                        supertypes.Add(results.Skip(i).First().supertype);
                                    }
                                }
                                //Alphabetize supertype list
                                supertypes.Sort();

                                //Push supertype list to supertype tab
                                Tab_supertypes.ItemsSource = supertypes;
                                Tab_supertypes.Items.Refresh();

                                //Access availible supertype
                                Tab_supertypes.SelectedValue = col.FindOne(x => x != null).supertype;
                            }
                        }
                        else
                        {
                            //Access availible type
                            Tab_types.SelectedValue = col.FindOne(x => x.supertype == supertype).type;
                        }
                    }
                    else
                    {
                        //Access availible subtype
                        subtypes.Remove(subtype);
                        Tab_subtypes.ItemsSource = subtypes;
                        Tab_subtypes.Items.Refresh();
                        Tab_subtypes.SelectedValue = col.FindOne(x => x.type == type).subtype;
                    }
                }
                else
                {
                    //Access availible slide
                    slide_current = col.FindOne(x => x.subtype == subtype).slide_num;
                    push_slide();
                    slide_button_lock();
                }
            }
        }

        /// <summary>
        /// Refresh UI with specific slide
        /// </summary>
        /// <param name="targetSlide"></param>
        private void Refresh(Slide targetSlide)
        {
            //Access Database file
            using (var db = new LiteDatabase(directory))
            {
                //Load database to collection
                var col = db.GetCollection<Slide>("note");

                //Test if given slide does not exists
                if (col.FindById(targetSlide.Id) == null)
                {
                    //Set supertype to default
                    Tab_supertypes.SelectedIndex = 0;
                }
                else
                {
                    //Clear Supertypes tab
                    supertypes.Clear();

                    //Get all distinct supertypes
                    var results = col.FindAll();

                    //Populate supertype list
                    for (int i = 0; i < results.Count(); i++)
                    {
                        //Test if supertype is unique
                        if (!supertypes.Contains((results.Skip(i).First().supertype)))
                        {
                            //Add supertype from DB to list of supertypes
                            supertypes.Add(results.Skip(i).First().supertype);
                        }
                    }
                    //Alphabetize supertype list
                    supertypes.Sort();

                    //Push supertype list to supertype tab
                    Tab_supertypes.ItemsSource = supertypes;
                    Tab_supertypes.Items.Refresh();

                    //Load updated Slide
                    Tab_supertypes.SelectedValue = targetSlide.supertype;
                    Tab_types.SelectedValue = targetSlide.type;
                    Tab_subtypes.SelectedValue = targetSlide.subtype;
                    slide_current = targetSlide.slide_num;

                    //Test if Selected Supertype is Valid
                    if (Tab_supertypes.SelectedIndex != -1)
                    {
                        //Test if Selected Type is Valid
                        if (Tab_types.SelectedIndex != -1)
                        {
                            //Test if Selected Subtype is Valid
                            if (Tab_subtypes.SelectedIndex != -1)
                            {
                                //Select update Slides number
                                slide_current = targetSlide.slide_num;
                                
                                //Update Slide button settings
                                slide_button_lock();

                                //Load slide
                                push_slide();
                            }
                            else
                            {
                                //Rebuild Supertype list
                                subtypes.Add(targetSlide.subtype);
                                subtypes.Sort();
                                Tab_subtypes.ItemsSource = subtypes;
                                Tab_subtypes.Items.Refresh();
                                Tab_subtypes.SelectedValue = targetSlide.subtype;
                            }
                        }
                        else
                        {
                            //Rebuild Type list
                            types.Add(targetSlide.type);
                            types.Sort();
                            Tab_types.ItemsSource = types;
                            Tab_types.Items.Refresh();
                            Tab_types.SelectedValue = targetSlide.type;
                        }
                    }
                    else 
                    {
                        //Rebuild Subtype list
                        supertypes.Add(targetSlide.supertype);
                        supertypes.Sort();
                        Tab_supertypes.ItemsSource = supertypes;
                        Tab_supertypes.Items.Refresh();
                        Tab_supertypes.SelectedValue = targetSlide.supertype;
                    }
                }
            }
        }
    }
}
