﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NotesInterface
{
    public partial class ConnStringChooser : Window
    {
        /// <summary>
        /// Initialize Connection String Chooser
        /// </summary>
        public ConnStringChooser()
        {   InitializeComponent();  }

        /// <summary>
        /// Close dialog (true)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pushString(object sender, RoutedEventArgs e)
        {   this.DialogResult = true;   }
        
        /// <summary>
        /// Get DB url path
        /// </summary>
        public string net
        {   get {   return netString.Text.ToString();   }   }
    }
}
