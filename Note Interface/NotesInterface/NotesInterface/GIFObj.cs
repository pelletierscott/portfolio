﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace NotesInterface
{
    /// <summary>
    /// Storage object for .gif type slide
    /// </summary>
    public class GIFObj
    {
        public List<Image> GIFImages = new List<Image>();
        public int GIFFrame = 0;
        public Image GIFElement;
        public int column;
    }
}