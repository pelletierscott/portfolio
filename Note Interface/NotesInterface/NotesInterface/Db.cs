﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;

namespace NotesInterface
{
    public class Db
    {
        //Declare variables
        String CONN_STRING = "";

        /// <summary>
        /// Get DB file from repository
        /// </summary>
        public Db()
        {
            //Load url for default DB file in repository
            string sourceURL = "https://bitbucket.org/pelletierscott/portfolio/raw/c49f4d3de9ef62db94139248b4d94d0a240eb9bd/Note%20Interface/defaultDB.db";

            //Create and open save file dialog
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.FileName = "defaultDB";
            saveFileDialog.DefaultExt = ".db";
            saveFileDialog.Filter = "Database File (.db)|*.db";
            saveFileDialog.ValidateNames = false;

            //Test if dialog worked
            if (saveFileDialog.ShowDialog() == true)
            {
                //Generate file steam profile
                string destinationPath = saveFileDialog.FileName;
                long fileSize = 0;
                int bufferSize = 1024;
                bufferSize *= 1000;
                long existLen = 0;
                System.IO.FileStream saveFileStream;
                
                //Test if target file exists
                if (System.IO.File.Exists(destinationPath))
                {
                    //Get file metadata
                    System.IO.FileInfo destinationFileInfo = new System.IO.FileInfo(destinationPath);
                    existLen = destinationFileInfo.Length;
                }

                //Try transfering file
                try
                {
                    //Create and Send HTTP request
                    System.Net.HttpWebRequest httpReq;
                    System.Net.HttpWebResponse httpRes;
                    httpReq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(sourceURL);
                    httpReq.AddRange((int)existLen);
                    System.IO.Stream resStream;
                    httpRes = (System.Net.HttpWebResponse)httpReq.GetResponse();

                    //Get HTTP response data
                    resStream = httpRes.GetResponseStream();
                    fileSize = httpRes.ContentLength;

                    //Create File Stream
                    saveFileStream = new System.IO.FileStream(destinationPath,
                                                                System.IO.FileMode.Create,
                                                                System.IO.FileAccess.Write,
                                                                System.IO.FileShare.ReadWrite);

                    //Create byte array for file storage
                    int byteSize;
                    byte[] downBuffer = new byte[bufferSize];

                    //Loop through and assign bytes to array
                    while ((byteSize = resStream.Read(downBuffer, 0, downBuffer.Length)) > 0)
                    {
                        saveFileStream.Write(downBuffer, 0, byteSize);
                    }

                    //Close file stream
                    saveFileStream.Close();

                    //Get DB file path
                    CONN_STRING = saveFileDialog.FileName;
                }
                catch (Exception ex)
                {
                    //Inform user of unsupported file type
                    MessageBox.Show("The file you selected might not be supported. Unrecognised file types will be intepreted as plain text (.txt)");
                }
            } 
        }

        //Return DB file path
        public string Conn
        {
            get { return CONN_STRING; }
        }
    }
}