﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NotesInterface
{
    /// <summary>
    /// Choose a connection to a DB file
    /// </summary>
    public partial class ConnectionChooser : Window
    {
        //Declare variables
        string CONN_STRING;

        /// <summary>
        /// Initialize dialog window - Connection Chooser
        /// </summary>
        public ConnectionChooser()
        {   InitializeComponent();  }

        /// <summary>
        /// Find a file in the local machine
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void findDb(object sender, RoutedEventArgs e)
        {
            //Create and run file open dialog
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "";
            dlg.DefaultExt = ".db";
            dlg.Filter = "Database File (.db)|*.db";
            dlg.ValidateNames = false;
            dlg.ShowDialog();

            //Test if file was selected
            if (dlg.FileName != "")
            {
                //Assign 
                CONN_STRING = dlg.FileName;
                this.DialogResult = true;
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// Load default Db file from github
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void newDb(object sender, RoutedEventArgs e)
        {
            //Create new DB object
            Db db = new Db();

            //Test if DB has connection path
            if (db.Conn != "")
            {
                //Get and return DB file path
                CONN_STRING = db.Conn;
                this.DialogResult = true;
                return;
            }
        }

        /// <summary>
        /// Access DB from url
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void manualDb(object sender, RoutedEventArgs e)
        {
            //Create and open Connection String Chooser dialog
            ConnStringChooser netString = new ConnStringChooser();
            netString.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            //Test if dialog worked
            if (netString.ShowDialog() == true)
            {
                //Test if dialog return is valid
                if (netString.net.Length != 0)
                {
                    //Create and open save file dialog
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.FileName = "externalDB";
                    saveFileDialog.DefaultExt = ".db";
                    saveFileDialog.Filter = "Database File (.db)|*.db";
                    saveFileDialog.ValidateNames = false;

                    //Test if dialog worked
                    if (saveFileDialog.ShowDialog() == true)
                    {
                        //Generate file steam profile
                        string destinationPath = saveFileDialog.FileName;
                        long fileSize = 0;
                        int bufferSize = 1024;
                        bufferSize *= 1000;
                        long existLen = 0;
                        System.IO.FileStream saveFileStream;

                        //Test if target file exists
                        if (System.IO.File.Exists(destinationPath))
                        {
                            //Get file metadata
                            System.IO.FileInfo destinationFileInfo = new System.IO.FileInfo(destinationPath);
                            existLen = destinationFileInfo.Length;
                        }

                        //Try transfering file
                        try
                        {
                            //Create and Send HTTP request
                            System.Net.HttpWebRequest httpReq;
                            System.Net.HttpWebResponse httpRes;
                            httpReq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(netString.net);
                            httpReq.AddRange((int)existLen);
                            System.IO.Stream resStream;
                            httpRes = (System.Net.HttpWebResponse)httpReq.GetResponse();

                            //Get HTTP response data
                            resStream = httpRes.GetResponseStream();
                            fileSize = httpRes.ContentLength;

                            //Create File Stream
                            saveFileStream = new System.IO.FileStream(destinationPath,
                                                                        System.IO.FileMode.Create,
                                                                        System.IO.FileAccess.Write,
                                                                        System.IO.FileShare.ReadWrite);

                            //Create byte array for file storage
                            int byteSize;
                            byte[] downBuffer = new byte[bufferSize];

                            //Loop through and assign bytes to array
                            while ((byteSize = resStream.Read(downBuffer, 0, downBuffer.Length)) > 0)
                            {
                                saveFileStream.Write(downBuffer, 0, byteSize);
                            }

                            //Close file stream
                            saveFileStream.Close();

                            //Get DB file path
                            CONN_STRING = saveFileDialog.FileName;
                        }
                        catch (Exception ex)
                        {
                            //Inform user of unsupported file type
                            MessageBox.Show("The file you selected might not be supported. Unrecognised file types will be intepreted as plain text (.txt)");
                        }
                    }

                    //Return that dialog was successful
                    this.DialogResult = true;
                }
            }
        }

        /// <summary>
        /// Return DB file path
        /// </summary>
        public string Conn
        {   get {   return CONN_STRING; }   }
    }
}
