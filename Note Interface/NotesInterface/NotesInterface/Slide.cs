﻿namespace NotesInterface
{
    /// <summary>
    /// Slide POCO
    /// </summary>
    public class Slide
    {
        public int Id { get; set; }
        public string supertype { get; set; }
        public string type { get; set; }
        public string subtype { get; set; }
        public int slide_num { get; set; }
        public string slideName { get; set; }
        public string fileName1 { get; set; }
        public byte[] file1 { get; set; }
        public string fileName2 { get; set; }
        public byte[] file2 { get; set; }
    }
}