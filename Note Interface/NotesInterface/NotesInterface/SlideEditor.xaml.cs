﻿using LiteDB;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Path = System.IO.Path;

namespace NotesInterface
{
    /// <summary>
    /// Add or Modify information of a stored slide
    /// </summary>
    public partial class SlideEditor : Window, INotifyCollectionChanged
    {
        //Declare variables
        List<string> supertypes_list = new List<string>();
        List<string> types_list = new List<string>();
        List<string> subtypes_list = new List<string>();
        List<string> slide_list = new List<string>();
        public Slide slideToUpdate;
        public Slide newSlide;
        string directory;
        string leftDir = "";
        string rightDir = "";
        int update_id;
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Add Slide Form Setup
        /// </summary>
        /// <param name="directory"></param>
        public SlideEditor(string dir)
        {
            //Assign directory
            directory = dir;

            //Add blank supertype template to list
            supertypes_list.Add("");

            //Get all supertype templates from database
            using (var db = new LiteDatabase(directory))
            {
                // Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Slide>("note");

                //Get all supertypes from database
                var results = col.FindAll();

                //Populate supertype list
                for (int i = 0; i < results.Count(); i++)
                {
                    //Test if supertype is unique
                    if (!supertypes_list.Contains((results.Skip(i).First().supertype)))
                    {
                        //Add supertype from database to list of supertypes
                        supertypes_list.Add(results.Skip(i).First().supertype);
                    }
                }
                //Alphabetize supertype list
                supertypes_list.Sort();
            }
            //Initialize form
            InitializeComponent();

            //Modify submit button
            bt_submit.Content = "ADD SLIDE";
            bt_submit.Click += Proceed_Add;

            //Bind supertypes to source
            cb_supertype.IsEnabled = true;
            cb_supertype.ItemsSource = supertypes_list;
            cb_supertype.SelectedIndex = 0;
        }

        /// <summary>
        /// Edit Slide Form Setup
        /// </summary>
        /// <param name="directory"></param>
        /// <param name="id"></param>
        public SlideEditor(string dir, int id)
        {
            //Declare variables
            directory = dir;
            update_id = id;

            //Add blank supertype template to list
            supertypes_list.Add("");

            //Get all supertype templates from database
            using (var db = new LiteDatabase(directory))
            {
                //Get a collection (or create, if doesn't exist)
                var col = db.GetCollection<Slide>("note");

                //Load Slide to edit
                var slideNeeded = col.Find(x => x.Id == update_id);

                //Map database slide to Slide instance
                slideToUpdate = new Slide()
                {
                    Id = update_id,
                    supertype = slideNeeded.First().supertype,
                    type = slideNeeded.First().type,
                    subtype = slideNeeded.First().subtype,
                    slide_num = slideNeeded.First().slide_num,
                    slideName = slideNeeded.First().slideName,
                    fileName1 = slideNeeded.First().fileName1,
                    file1 = slideNeeded.First().file1,
                    fileName2 = slideNeeded.First().fileName2,
                    file2 = slideNeeded.First().file2
                };
                
                //Get all supertypes from database
                var results = col.FindAll();

                //Populate supertype list
                for (int i = 0; i < results.Count(); i++)
                {
                    //Test if supertype is unique
                    if (!supertypes_list.Contains((results.Skip(i).First().supertype)))
                    {
                        //Add supertype from database to list of supertypes
                        supertypes_list.Add(results.Skip(i).First().supertype);
                    }
                }
                //Alphabetize supertype list
                supertypes_list.Sort();
            }

            //Initialize form
            InitializeComponent();

            //Update submit button
            bt_submit.Content = "UPDATE SLIDE";
            bt_submit.Click += Proceed_Update;

            //Bind supertypes to source
            cb_supertype.IsEnabled = true;
            cb_supertype.ItemsSource = supertypes_list;
        }

        /// <summary>
        /// Edit Slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Editor_Loaded(object sender, RoutedEventArgs e)
        {
            //Test if form is configured to edit Slides
            if (bt_submit.Content.ToString() == "UPDATE SLIDE")
            {
                //Send Slide data to form
                cb_supertype.Text = slideToUpdate.supertype;
                cb_type.Text = slideToUpdate.type;
                cb_subtype.Text = slideToUpdate.subtype;
                slide_order.Text = slideToUpdate.slide_num.ToString();
                slideTitleBox.Text = slideToUpdate.slideName;
                slide_order.SelectedIndex = slideToUpdate.slide_num;

                //Setup left file panel
                leftFileFinder.Foreground = Brushes.Green;
                leftFileFinder.Content = "\u2714";
                leftFileFinder.ToolTip = "<" + slideToUpdate.slideName + " - L>" + slideToUpdate.fileName1;
                leftDir = "<STORED>";

                //Test if right file panel needs to be setup
                if (slideToUpdate.fileName2 == "void")
                {
                    //Setup right file panel
                    rightFileFinder.ToolTip = "";
                    rightDir = "";
                    displaycheck.IsChecked = false;
                }
                else
                {
                    //Set right panel as blank
                    rightFileFinder.ToolTip = "<" + slideToUpdate.slideName + " - R>" + slideToUpdate.fileName2;
                    rightDir = "<STORED>";
                    displaycheck.IsChecked = true;
                }
            }
        }

        /// <summary>
        /// Modify form after supertype change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_supertype_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Get new text
            string supertype_text = cb_supertype.Text.ToString();

            //Test if text is blank/empty
            if (supertype_text.Trim() == "")
            {
                //Clear types list
                types_list.Clear();

                //Alphabetize type list
                types_list.Sort();

                //Bind supertypes to source
                cb_type.ItemsSource = types_list;
                cb_type.IsEnabled = false;
            }
            else
            {
                //Test if there is a supertype match
                if (supertypes_list.Contains(supertype_text))
                {
                    //Select the matching supertype
                    cb_supertype.SelectedIndex = cb_supertype.Items.IndexOf(supertype_text);

                    //Clear supertype list
                    types_list.Clear();

                    //Get all type templates from database
                    using (var db = new LiteDatabase(directory))
                    {
                        // Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all types belonging to supertype from database
                        var results = col.Find(x => x.supertype == supertype_text);

                        //Populate type list
                        for (int i = 0; i < results.Count(); i++)
                        {
                            //Test if type is unique
                            if (!types_list.Contains((results.Skip(i).First().type)))
                            {
                                //Add type from database to list of types
                                types_list.Add(results.Skip(i).First().type);
                            }
                        }
                    }
                    //Add blank type template to list
                    types_list.Add("");

                    //Alphabetize type list
                    types_list.Sort();

                    //Bind supertypes to source
                    cb_type.IsEnabled = true;
                    //cb_type.
                    cb_type.ItemsSource = types_list;
                    cb_type.SelectedIndex = 0;
                }
                else
                {
                    //Clear types list
                    types_list.Clear();

                    //Add blank type template to list
                    types_list.Add("");

                    //Alphabetize type list
                    types_list.Sort();

                    //Bind supertypes to source
                    cb_type.IsEnabled = true;
                    cb_type.ItemsSource = types_list;
                    cb_type.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Modify form after type change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_type_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Get new text
            string supertype_text = cb_supertype.Text.ToString();
            string type_text = cb_type.Text.ToString();

            //Test if text is blank/empty
            if (type_text.Trim() == "")
            {
                //Clear types list
                subtypes_list.Clear();

                //Alphabetize type list
                subtypes_list.Sort();

                //Bind supertypes to source
                cb_subtype.ItemsSource = subtypes_list;
                cb_subtype.IsEnabled = false;
            }
            else
            {
                //Test if there is a type match
                if (types_list.Contains(type_text))
                {
                    //Select the matching supertype
                    cb_type.SelectedIndex = cb_subtype.Items.IndexOf(type_text);

                    //Clear subtypes list
                    subtypes_list.Clear();

                    //Get all type templates from database
                    using (var db = new LiteDatabase(directory))
                    {
                        // Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all types belonging to supertype from database
                        var results = col.Find(x => x.type == type_text &&
                                                    x.supertype == supertype_text);

                        //Populate type list
                        for (int i = 0; i < results.Count(); i++)
                        {
                            //Test if type is unique
                            if (!subtypes_list.Contains((results.Skip(i).First().subtype)))
                            {
                                //Add type from database to list of types
                                subtypes_list.Add(results.Skip(i).First().subtype);
                            }
                        }
                    }
                    //Add blank type template to list
                    subtypes_list.Add("");

                    //Alphabetize type list
                    subtypes_list.Sort();

                    //Bind supertypes to source
                    cb_subtype.IsEnabled = true;
                    cb_subtype.ItemsSource = subtypes_list;
                    cb_subtype.SelectedIndex = 0;
                }
                else
                {
                    //Clear types list
                    subtypes_list.Clear();

                    //Add blank type template to list
                    subtypes_list.Add("");

                    //Alphabetize type list
                    subtypes_list.Sort();

                    //Bind supertypes to source
                    cb_subtype.IsEnabled = true;
                    cb_subtype.ItemsSource = subtypes_list;
                    cb_subtype.SelectedIndex = 0;
                    cb_subtype.Items.Refresh();       
                }
            }
        }

        /// <summary>
        /// Modify form after subtype change
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cb_subtype_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Get new text
            string supertype_text = cb_supertype.Text.ToString();
            string type_text = cb_type.Text.ToString();
            string subtype_text = cb_subtype.Text.ToString();

            //Test if text is blank/empty
            if (subtype_text.Trim() == "")
            {
                //Disable all subjunctive controls
                displaycheck.IsEnabled = false;
                slide_order.IsEnabled = false;
                slideTitleBox.IsEnabled = false;
                leftFileFinder.IsEnabled = false;
                rightFileFinder.IsEnabled = false;
            }
            else
            {
                //Test if there is matching subtype
                if (subtypes_list.Contains(subtype_text))
                {
                    //Enable all subjunctive controls
                    displaycheck.IsEnabled = true;
                    slide_order.IsEnabled = true;
                    slideTitleBox.IsEnabled = true;
                    leftFileFinder.IsEnabled = true;

                    //Test if a file for left/center panel is selected
                    if (leftDir == "")
                    {
                        //Change button to indicate no file (negative)
                        leftFileFinder.Content = "X";
                        leftFileFinder.Foreground = Brushes.Red;
                    }

                    //Test if a file for right panel is and can be selected
                    if (rightDir == "" && displaycheck.IsChecked == true)
                    {
                        //Change button to indicate no file (negative)
                        rightFileFinder.Content = "X";
                        rightFileFinder.Foreground = Brushes.Red;
                    }

                    //Clear and set default value of slide list
                    slide_list.Clear();
                    slide_list.Add("...First");

                    //Pull data to populate slide list
                    using (var db = new LiteDatabase(directory))
                    {
                        //Get a collection (or create, if doesn't exist)
                        var col = db.GetCollection<Slide>("note");

                        //Get all matching slides of same subtype
                        var results = col.Find(x =>
                            x.supertype == supertype_text &&
                            x.type == type_text &&
                            x.subtype == subtype_text);

                        //Create structure for sorting slide names
                        List<string> slideNames = new List<string>();

                        //Loop through all matching slides (first to last)
                        for (int i = 0; i < results.Count(); i++)
                        {
                            //Loop through matching slides (0 - Nth)
                            for (int j = 0; j < results.Count(); j++)
                            {
                                //Test if user is updating or adding (skip adding current to list)
                                if (slideToUpdate == null || results.Skip(j).First().slideName != slideToUpdate.slideName)
                                {
                                    //Test if slide matches numerical order
                                    if (results.Skip(j).First().slide_num == i)
                                    {
                                        //Add slide's name to list
                                        slideNames.Add(results.Skip(j).First().slideName);    
                                    }
                                }
                            }
                        }

                        //Loop through each matching Slide
                        for (int i = 0; i < slideNames.Count(); i++)
                        {
                            //Add Slide's name to list 
                            slide_list.Add((i + 1) + " - After " + slideNames[i]);   
                        }
                    }
                    //Update Slide name list
                    slide_order.ItemsSource = slide_list;
                    slide_order.Items.Refresh();
                }
                else
                {
                    //Enable all subjunctive controls
                    displaycheck.IsEnabled = true;
                    slide_order.IsEnabled = true;
                    slideTitleBox.IsEnabled = true;
                    leftFileFinder.IsEnabled = true;
                    
                    //Change button to indicate no file (negative)
                    leftFileFinder.Content = "X";
                    leftFileFinder.Foreground = Brushes.Red;

                    //Create new Slide Name list and update
                    slide_list.Clear();
                    slide_list.Add("...First");
                    slide_order.ItemsSource = slide_list;
                    slide_order.Items.Refresh();
                }
            }
        }

        /// <summary>
        /// Toggle two panel display (ON)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void displaycheck_Checked(object sender, RoutedEventArgs e)
        {
            //Enable right panel file picker
            rightFileFinder.IsEnabled = true;

            //Test if a file is selected
            if (rightDir != "")
            {
                //Change button to indicate a file (positive)
                rightFileFinder.Content = "\u2714";
                rightFileFinder.Foreground = Brushes.Green;
            }
            else
            {
                //Change button to indicate no file (negative)
                rightFileFinder.Content = "X";
                rightFileFinder.Foreground = Brushes.Red;
            }
        }

        /// <summary>
        /// Toggle two panel display (OFF) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void displaycheck_Unchecked(object sender, RoutedEventArgs e)
        {
            //Disable and purge right panel contents
            rightFileFinder.IsEnabled = false;
            rightFileFinder.Content = "";
        }

        /// <summary>
        /// Get file for slide panels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void FindFile(object sender, RoutedEventArgs e)
        {
            //Create and open dialog
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = "";
            dlg.DefaultExt = ".txt";
            dlg.Filter = "" +
                "Text File (.txt)|*.txt|" +
                "PNG File (.png)|*.png|" +
                "Bitmap (.bmp)|*.bmp|" +
                "JPG (.jpg)|*.jpg|" +
                "Graphical Image File (.gif)|" +
                "*.png|All files (*.*)|" +
                "*.*";
            dlg.FilterIndex = 6;
            dlg.ValidateNames = false;

            //Test if dialog returned a file
            if (dlg.ShowDialog() == true)
            {
                //Load file information
                string testDirectory = dlg.FileName;
                FileInfo fi = new FileInfo(testDirectory);

                //Test file extension against supported types
                if (fi.Extension != ".txt" && 
                    fi.Extension != ".png" &&
                    fi.Extension != ".bmp" &&
                    fi.Extension != ".jpg" &&
                    fi.Extension != ".jpeg" &&
                    fi.Extension != ".gif")
                {
                    //Handle Unknown File Types
                    MessageBox.Show("The file you selected isn't a supported type. \n " +
                        "This will be displayed as plain text");
                }

                //Test left and right directories: if source override else keep value
                leftDir = (e.OriginalSource == leftFileFinder) ? testDirectory : leftDir;
                rightDir = (e.OriginalSource == rightFileFinder) ? testDirectory : rightDir;

                //Test left directory
                if (leftDir != "")
                {
                    //Positive
                    leftFileFinder.Foreground = Brushes.Green;
                    leftFileFinder.Content = "\u2714";
                    leftFileFinder.ToolTip = fi.Name;
                }
                else
                {
                    //Negative
                    leftFileFinder.Foreground = Brushes.Red;
                    leftFileFinder.Content = "X";
                    leftFileFinder.ToolTip = null;
                }

                //Test if double panel is selected
                if (displaycheck.IsChecked == true)
                {
                    //Test right directory 
                    if (rightDir != "")
                    {
                        //Positive
                        rightFileFinder.Foreground = Brushes.Green;
                        rightFileFinder.Content = "\u2714";
                        rightFileFinder.ToolTip = fi.Extension;
                        rightFileFinder.ToolTip = fi.Name;
                    }
                    else
                    {
                        //Negative
                        rightFileFinder.Foreground = Brushes.Red;
                        rightFileFinder.Content = "X";
                        rightFileFinder.ToolTip = null;
                    }
                }
            } 
        }

        /// <summary>
        /// Configure Slide title textbox (Focus)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slideTitleBox_GotFocus(object sender, RoutedEventArgs e)
        {
            //Test if textbox is empty
            if (slideTitleBox.Text.ToString() == "Slide Title")
            {
                //Remove placeholder text
                slideTitleBox.Foreground = Brushes.Black;
                slideTitleBox.Text = "";
            }
        }

        /// <summary>
        /// Configure Slide title textbox (Unfocus)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void slideTitleBox_LostFocus(object sender, RoutedEventArgs e)
        {
            //Test if textbox is empty
            if (((string)slideTitleBox.Text).ToString().Length == 0)
            {
                //Add placeholder text
                slideTitleBox.Text = "Slide Title";
                slideTitleBox.Foreground = Brushes.Gray;
            }
        }

        /// <summary>
        /// Add a new Slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Proceed_Add(object sender, RoutedEventArgs e)
        {
            //Validate Data
            string str = Validate();

            //Test validation
            if (str.Length == 0)
            {
                //Access Database file
                using (var db = new LiteDatabase(directory))
                {
                    // Get a collection (or create, if doesn't exist)
                    var col = db.GetCollection<Slide>("note");

                    //Configure New Slide Data
                    string t_supertype = cb_supertype.Text.ToString();
                    string t_type = cb_type.Text.ToString();
                    string t_subtype = cb_subtype.Text.ToString();
                    int t_slide_num = slide_order.SelectedIndex;
                    string t_slideName = slideTitleBox.Text.ToString();
                    string t_fileName1 = Path.GetExtension(leftDir);
                    byte[] t_file1 = System.IO.File.ReadAllBytes(leftDir);
                    string t_fileName2 = "";
                    byte[] t_file2 = null;

                    //Test if right file is set or not
                    if (rightDir.Length == 0)
                    {
                        //Set right file as empty
                        t_fileName2 = "void";
                        t_file2 = null;
                    }
                    else
                    {
                        //Load right file to byte array
                        t_fileName2 = Path.GetExtension(rightDir);
                        t_file2 = System.IO.File.ReadAllBytes(rightDir);
                    }

                    //Resequence Slides
                    //Get all slide is same subtype
                    var results = col.Find(x =>
                        x.supertype.Equals(t_supertype) &&
                        x.type.Equals(t_type) &&
                        x.subtype.Equals(t_subtype));

                    //Loop through each matching slide
                    foreach (Slide slide in results)
                    {
                        //Test if slide is effected
                        if (slide.slide_num >= t_slide_num)
                        {
                            //Modify slide order
                            slide.slide_num += 1;
                            col.Update(slide);
                        }
                    }

                    //Create Slide
                    newSlide = new Slide
                    {
                        supertype = t_supertype,
                        type = t_type,
                        subtype = t_subtype,
                        slide_num = t_slide_num,
                        slideName = t_slideName,
                        fileName1 = t_fileName1,
                        file1 = t_file1,
                        fileName2 = t_fileName2,
                        file2 = t_file2
                    };

                    //Insert new Slide
                    col.Insert(newSlide);
                }
                this.DialogResult = true;
            }
            else
            {
                //Show Form errors
                MessageBox.Show("Form was not completed: \n\n" + str);
            }   
        }

        /// <summary>
        /// Update a Slide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Proceed_Update(object sender, RoutedEventArgs e)
        {
            //Validate Data
            string str = Validate();

            //Test validation
            if (str.Length == 0)
            {
                using (var db = new LiteDatabase(directory))
                {
                    // Get a collection (or create, if doesn't exist)
                    var col = db.GetCollection<Slide>("note");

                    //Configure New Slide Data
                    string t_supertype = cb_supertype.Text.ToString();
                    string t_type = cb_type.Text.ToString();
                    string t_subtype = cb_subtype.Text.ToString();
                    int t_slide_num = slide_order.SelectedIndex;
                    string t_slideName = slideTitleBox.Text.ToString();
                    string t_fileName1;
                    byte[] t_file1;

                    //Test if left panel file has changed
                    if (leftDir == "<STORED>")
                    {
                        //Use stored file data
                        t_fileName1 = slideToUpdate.fileName1;
                        t_file1 = slideToUpdate.file1;
                    }
                    else
                    {
                        //Overwrite left file data
                        t_fileName1 = Path.GetExtension(leftDir);
                        t_file1 = System.IO.File.ReadAllBytes(leftDir);
                    }

                    //Declare right file variables
                    string t_fileName2 = "";
                    byte[] t_file2 = null;

                    //Test if right panel file has changed
                    if (rightDir == "<STORED>")
                    {
                        //Use stored file data
                        t_fileName2 = slideToUpdate.fileName2;
                        t_file2 = slideToUpdate.file2;
                    }
                    else
                    {
                        //Test if right panel is set
                        if (rightDir.Length == 0)
                        {
                            //Set right file as empty
                            t_fileName2 = "void";
                            t_file2 = null;
                        }
                        else
                        {
                            //Load right file to byte array
                            t_fileName2 = Path.GetExtension(rightDir);
                            t_file2 = System.IO.File.ReadAllBytes(rightDir);
                        }
                    }

                    //Test if Slide order need to be resequenced
                    if (slideToUpdate.slide_num != slide_order.SelectedIndex)
                    {
                        //Get all slide is same subtype
                        var results = col.Find(x =>
                        x.supertype.Equals(t_supertype) &&
                        x.type.Equals(t_type) &&
                        x.subtype.Equals(t_subtype));

                        //Loop through each matching slide
                        foreach (Slide slide in results)
                        {
                            //Test if slide is effected
                            if (slide.slide_num >= t_slide_num)
                            {
                                //Modify slide order
                                slide.slide_num += 1;
                                col.Update(slide);
                            }
                        }
                    }

                    //Assign form information to Slide instance
                    newSlide = new Slide
                    {
                        Id = slideToUpdate.Id,
                        supertype = t_supertype,
                        type = t_type,
                        subtype = t_subtype,
                        slide_num = t_slide_num,
                        slideName = t_slideName,
                        fileName1 = t_fileName1,
                        file1 = t_file1,
                        fileName2 = t_fileName2,
                        file2 = t_file2
                    };

                    //Insert new Slide
                    slideToUpdate = newSlide;
                    col.Update(slideToUpdate);
                }
                this.DialogResult = true;
            }
            else
            {
                //Show Form errors
                MessageBox.Show("Form was not completed: \n\n" + str);
            }
        }

        /// <summary>
        /// Validate form's fields
        /// </summary>
        /// <returns></returns>
        private string Validate()
        {
            //Create error list string
            string str = "";

            //Test Slide Editor fields for errors
            str += (cb_supertype.Text.ToString().Length == 0 || cb_supertype.Text.ToString() is null) ? "Invalid SuperType\n" : "";
            str += (cb_type.Text.ToString().Length == 0 || cb_type.Text.ToString() is null) ? "Invalid Type\n" : "";
            str += (cb_subtype.Text.ToString().Length == 0 || cb_subtype.Text.ToString() is null) ? "Invalid SubType\n" : "";
            str += (slide_order.SelectedIndex == -1) ? "Invalid Slide Order\n" : "";
            str += (slideTitleBox.Text.ToString().Length == 0 || slideTitleBox.Text.ToString() is null) ? "Invalid Slide Title\n" : "";
            str += (leftDir.Length == 0) ? "Invalid Directory for File (Left)\n" : "";

            //Test if right panel directory need to be tested
            if (displaycheck.IsChecked == true)
            {
                //Test right panel directory for errors
                str += (rightDir.Length == 0) ? "Invalid Directory for File (Right)\n" : "";
            }

            //Return list of errors
            return str;
        }
    }
}
