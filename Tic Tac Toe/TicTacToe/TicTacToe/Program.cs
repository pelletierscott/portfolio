﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;



namespace TicTacToe
{

    class Program
    {
        //GAME LOOP
        static void Main(string[] args)
        {
            //INITIALIZE VARIABLES
            int player1_score = 0;
            int player2_score = 0;
            int curr_player = -1;
            int[][] gameboard = new int[][]
            {
                new int[] { 0, 0, 0},
                new int[] { 0, 0, 0},
                new int[] { 0, 0, 0}
            };
            char[][] displayboard = new char[][]
            {
                new char[] {' ',    '|',    ' ',    '|',    ' ' },
                new char[] {'_',    '+',    '_',    '+',    '_' },
                new char[] {' ',    '|',    ' ',    '|',    ' ' },
                new char[] {'_',    '+',    '_',    '+',    '_' },
                new char[] {' ',    '|',    ' ',    '|',    ' ' }
            };
            bool gameFlag = true;
            int tie = 0;

            //START GAME LOOP
            while (gameFlag)
            {
                //UPDATE PLAYER
                curr_player += 1;
                curr_player = curr_player % 2;

                //CYCLE ONE TURN
                tie += 1;
                rungame(player1_score, player2_score, curr_player, gameboard, displayboard);

                //TEST IF PLAYER HAS WON
                gameFlag = anotherRound(curr_player, gameboard);

                //TEST IF GAME IS A DRAW
                if (tie >= 9)
                { gameFlag = false; }

                //ADJUST SCORES
                if (!gameFlag)
                {
                    if (curr_player == 0)
                    { player1_score += scoreUpdate(gameboard); }
                    else
                    { player2_score += scoreUpdate(gameboard); }

                    //RESET BOARDS
                    gameboard = new int[][]
                    {
                        new int[] { 0, 0, 0},
                        new int[] { 0, 0, 0},
                        new int[] { 0, 0, 0}
                    };
                    displayboard = new char[][]
                    {
                        new char[] {' ',    '|',    ' ',    '|',    ' ' },
                        new char[] {'_',    '+',    '_',    '+',    '_' },
                        new char[] {' ',    '|',    ' ',    '|',    ' ' },
                        new char[] {'_',    '+',    '_',    '+',    '_' },
                        new char[] {' ',    '|',    ' ',    '|',    ' ' }
                    };
                    tie = 0;
                    curr_player = -1;

                    //END PROGRAM
                    Console.Write("\n\n  PLAY AGAIN? : PRESS 'Y' TO PLAY AGAIN\n");
                    ConsoleKeyInfo input = Console.ReadKey();
                    if (input.Key == ConsoleKey.Y)          
                    { gameFlag = !gameFlag; }               
                    else
                    { System.Environment.Exit(1); }         
                }
            }
        }

        //UPDATE SCORES
        private static int scoreUpdate(int[][] gameboard)
        {
            string testStr = "";                            //Create Test String For DRAW Check
            for (int i = 0; i < gameboard.Length; i++)      //Loop Through GameBoard
            {
                for (int j = 0; j < gameboard[i].Length; j++)
                {
                    if (gameboard[i][j] != 0)               //Test if Coordinate is Occupied
                    { testStr += 1; }                       //Draw 'Occupied' to Test String
                    else
                    { testStr += 0; }                       //Draw 'UnOccupied' to Test String
                }
            }

            if (!testStr.Contains("0"))                     //Test if Test String Yields A DRAW
            { return 0; }                                   //Return No Points

            return 1;                                       //Return One Point For Winner
        }

        //RUN ONE TURN OF TIC-TAC-TOE
        private static void rungame(int player1_score, int player2_score, int curr_player, int[][] gameboard, char[][] displayboard)
        {
            bool selected = true;                           //Set Default Player Turn Flag
            int xy_coord = 0;                               //Set Default Coordinate

            while (selected)                                //Loop While Turn is Ongoing
            {
                while (offsettest(xy_coord, gameboard))     //Loop Until An Empty Coordinate is Found
                { xy_coord += 1; }                          //Check Next Coordinate

                //Draw Game Data To Console
                displayUpdate(player1_score, player2_score, xy_coord, gameboard, displayboard);

                while (selected)                            //Loop While Turn is Ongoing
                {
                    //Capture User Keyboard Input
                    xy_coord = playerInput(curr_player, xy_coord, gameboard, displayboard);

                    //Update Game Data (Redraw Console)
                    displayUpdate(player1_score, player2_score, xy_coord, gameboard, displayboard);

                    if (xy_coord == -1)                     //Check for End of Turn Code
                    { selected = !selected; }               //Change Player Turn Flag
                }
            }
        }

        //CAPTURE USER INPUTS AND APPLY
        private static int playerInput(int curr_player, int xy_coord, int[][] gameboard, char[][] displayboard)
        {
            int offset = 0;                                 //Set Default Offset
            bool reverse = false;                           //Set Default Offset Direction
            ConsoleKeyInfo input = Console.ReadKey();       //Capture Keyboard Input

            switch (input.Key)                              //Process Keyboard Input
            {
                case ConsoleKey.UpArrow:
                { offset = xy_coord - 3; reverse = true; break; }

                case ConsoleKey.DownArrow:
                { offset = xy_coord + 3; reverse = false; break; }

                case ConsoleKey.LeftArrow:
                { offset = xy_coord - 1;reverse = true;break;}

                case ConsoleKey.RightArrow:
                { offset = xy_coord + 1; reverse = false; break; }

                case ConsoleKey.Enter:
                {
                    int x = xy_coord / 3;                   //Get Vertical Component
                    int y = xy_coord % 3;                   //Get Horizontal Component
                    gameboard[x][y] = curr_player + 1;      //Add Player Number to GameBoard

                    //Add Player Symbol to Display Board
                    displayboard[2 * x][2 * y] = (curr_player == 1) ? 'O' : 'X';
                    return -1;                              //Return Confirmation Code
                }
            }

            offset = offset % 9;                            //Roll-Over Offset

            //Validate Offset
            while (offset < 0 || offsettest(offset, gameboard))
            {
                if (offset < 0)                             //Test If Offset Is Out Of Range 
                { offset += 9; }                            //Clamp Offset (Upper Limit)

                while (offsettest(offset, gameboard))       //Test If Offset Is Out Of Range 
                { offset += (reverse) ? -1 : 1; }           //Clamp Offset (Lower Limit)
            }
            return offset;                                  //Return Validated Offset
        }

        //COMPARE GAMEBOARD TO WIN CONDITIONS
        private static bool anotherRound(int curr_player, int[][] gameboard)
        {
            string testStr = "";                            //Create Blank String for Test
            for (int i = 0; i < gameboard.Length; i++)      //Loop Through GameBoard
            {
                for (int j = 0; j < gameboard[i].Length; j++)   
                {
                    if (gameboard[i][j] == curr_player + 1) //Test for Current Player
                    { testStr += 1; }                       //Assign Positive Value
                    else
                    { testStr += 0; }                       //Assign Null Value
                }
            }

            switch (testStr)                                //Check for Game Winning Match
            {
                case ("111000000"):
                {   return false;   }
                case ("000111000"):
                { return false; }
                case ("000000111"):
                { return false; }
                case ("100100100"):
                { return false; }
                case ("010010010"):
                { return false; }
                case ("001001001"):
                { return false; }
                case ("100010001"):
                { return false; }
                case ("001010100"):
                { return false; }
                default:
                { return true; }                
            }
        }

        //WRITE ALL GAME INFORMATION TO CONSOLE
        private static void displayUpdate(int player1_score, int player2_score, int xy_coord, int[][] gameboard, char[][] displayboard)
        {
            Console.Clear();                                //Clear Previous Console
            printBanner();                                  //Print Banner
            printScores(player1_score, player2_score);      //Print Player Scores
            int x = xy_coord / 3;                           //Get Vertical Component                             
            int y = xy_coord % 3;                           //Get Horizontal Component 
            Console.Write("\n\n\n\n");                      //Add Spacer

            for (int i = 0; i < displayboard.Length; i++)   //Loop Through Board Positions
            {
                Console.Write("\t\t");                      //Indent Board
                for (int j = 0; j < displayboard[i].Length; j++)
                {
                    if (2 * x == i && 2 * y == j)           //Test for Selector
                    { Console.Write("?"); }                 //Print Selector Character (?)
                    else
                    { Console.Write(displayboard[i][j]); }  //Print Display Character

                    if (j == displayboard[i].Length - 1)    //Test for End of Line
                    { Console.Write("\n"); }                //Print New Line
                }
            }
        }

        //PRINT PLAYER SCORES
        private static void printScores(int player1_score, int player2_score)
        {
            Console.Write("\tX's : " + player1_score);
            Console.Write("\t\t");
            Console.Write("O's : " + player2_score);
        }

        //PRINT MAIN BANNER
        private static void printBanner()
        {
            Console.Write("\n");
            Console.Write("========================================\n");
            Console.Write("\t TIC - TAC - TOE \t\n");
            Console.Write("========================================\n");
            Console.Write("\n");
            Console.Write("\tBy: Scott Pelletier\n");
            Console.Write("\t1.0 : December 23rd 2018\n");
            Console.Write("\n\n\n");
        }

        //PUSH '?' INDICATOR TO NEXT AVAILIBLE SPACE
        private static bool offsettest(int xy_coord, int[][] gameboard)
        {
            xy_coord = xy_coord % 9;                        //Clamp Selection (Upper Limit)

            if (xy_coord < 0)                               //Clamp Selection (Lower Limit)
            { xy_coord = 8; }                               //Roll-over Selection

            int x = xy_coord / 3;                           //Get Vertical Component
            int y = xy_coord % 3;                           //Get Horizontal Component

            if (gameboard[x][y] != 0)                       //Test for Occupied Space
            { return true; }                                //:Space is Occupied

            return false;                                   //:Space is Unoccupied
        }
    }
}
